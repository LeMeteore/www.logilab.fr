#!/usr/bin/env python3
# coding: utf8

import sys
import os
import re
import glob
import requests
import argparse
from urllib.parse import urlparse
from unidecode import unidecode
from slugify import slugify
from datetime import datetime
from constants import (
    client,
    article_path,
    template_rest, template_html, templates,
    image_url, image_path, images_query,
    redirect_string_one, redirect_string_two,
    blog_entries_query, blog_keys,
    cards_query, card_keys,
)


def print_warning(msg):
    """ Custom print coloring printed message. """
    print(f'\x1b[1;33;40m {msg} \x1b[0m')

def my_download(url):
        global image_path
        local_path = None
        fname = os.path.basename(urlparse(url).path)
        try:
            r = requests.get(url, stream=True)
            if r.status_code == 200:
                if "Content-Disposition" in r.headers.keys():
                    fname = re.findall("filename=(.+)",
                                       r.headers["Content-Disposition"])[0]
                local_path = image_path.format(data_name=fname)
                with open(local_path, 'wb') as f:
                    for chunk in r.iter_content(1024):
                        f.write(chunk)
        except requests.exceptions.RequestException as exc:
            print(exc)
        return local_path


def update_articles():
    """ Replace images links inside article files. """

    for i, article in enumerate(glob.iglob("./all_articles/**/*.*", recursive=True)):
        with open(article, 'r') as f:
            content = f.read()

            # match only images from logilab
            match = re.search( r'\.\. ?image ?:: ?((https?://)?(www\.)?(logilab\.fr)?(/.*))', content, re.M|re.I)

            if match:
                orig_url = url = match.group(5)

                # add scheme if missing
                if url.startswith("//"):
                    url = "{}{}".format("https:", url)

                # add netloc if missing
                if url.startswith("/"):
                    url = "{}{}".format("https://www.logilab.fr", url)

                # download image
                local_path = my_download(url)
                if local_path:
                    content = content.replace(url, local_path)
                    with open(article, 'w') as f:
                        f.write(content)
                        # print_warning(f"Successfully replaced {orig_url} by {local_path} inside {f.name}")


def write_blogentry(blogentry, template, extension):
    """ Write blog entry to file on disk."""
    article = article_path.format(unidecode(blogentry['category'].lower()),
                                  blogentry['eid'],
                                  blogentry['slug'],
                                  extension)

    if not os.path.exists(os.path.dirname(article)):
        try:
            os.makedirs(os.path.dirname(article))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    with open(article, 'w+') as f:
        f.write(template.format(**blogentry))


def write_card(card, template, extension):
    """ Write Card to file on disk."""
    article = article_path.format(unidecode(card['category'].lower()),
                                  card['eid'],
                                  card['slug'],
                                  extension)

    if not os.path.exists(os.path.dirname(article)):
        try:
            os.makedirs(os.path.dirname(article))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    with open(article, 'w+') as f:
        f.write(template.format(**card))



def write_redirect(blogentry, creation_date):
    """ Write redirect for blog entry. """

    redirect1 = redirect_string_one.format(etype=blogentry['etype'],
                                           eid=blogentry['eid'],
                                           year=creation_date.year,
                                           month=creation_date.month,
                                           slug=blogentry['slug'])

    redirect2 = redirect_string_two.format(eid=blogentry['eid'],
                                           year=creation_date.year,
                                           month=creation_date.month,
                                           slug=blogentry['slug'])

    with open('redirects', 'a') as f:
        f.write(redirect1)
        f.write(redirect2)



def extract_blog_entries(blogentries):
    """ Article extraction + write of redirects. """

    for i, be in enumerate(blogentries):
        global blog_keys
        blogentry = dict(zip(blog_keys, be))
        template = templates[blogentry['content_format']]

        blogentry['underline'] = '#' * len(blogentry['title'])
        blogentry['etype'] = "BlogEntry"
        appendix = blogentry['content_format'].split('/')[1]
        extension = 'rst' if appendix == 'rest' else appendix
        creation_date = datetime.strptime(blogentry['creation_date'], '%Y/%m/%d %H:%M:%S')

        if blogentry['content']:
            blogentry['content'] = blogentry['content'].replace(':rql:', 'rql')

        if blogentry['title']:
            blogentry['slug'] = slugify(blogentry['title']).lower()
        else:
            blogentry['slug'] = blogentry['eid']

        write_blogentry(blogentry, template, extension)
        write_redirect(blogentry, creation_date)


def extract_images(resp):
    print_warning("Number of images found: {n}".format(n=len(resp.json())))
    print_warning("---")
    success = []
    errors = []

    global image_url, image_path

    if resp.status_code == 200:
        for i, (eid, data_name, cwuri) in enumerate(resp.json()):
            if (eid and data_name):
                download_url = image_url.format(eid=eid, data_name=data_name)
                # print_warning("The cwuri is: {}".format(cwuri))
                # print_warning("The image link is: {}".format(download_url))
                # print_warning('---')

                # download only if files not exists
                if not os.path.isfile(image_path.format(data_name=data_name)):
                    response = requests.get(download_url, stream=True)

                    if response.status_code == 200:
                        with open(image_path.format(data_name=data_name), 'wb') as f:
                            for chunk in response.iter_content(1024):
                                f.write(chunk)
                            # print_warning(f"{f.name} successfully written to disk")
                            success.append(f"{f.name} successfully written to disk")
                    else:
                        # print_warning("Error: {status} on url {url}".format(status=r.status_code,
                        #                                             url=download_url))
                        errors.append("Error: {status} on url {url}".format(status=r.status_code,
                                                                            url=download_url))
                else:
                    # print_warning(f"Image {image_path.format(data_name=data_name)} already exists, not downloading")
                    success.append(f"Image {image_path.format(data_name=data_name)} already exists, not downloaded")
            else:
                # print_warning('Something went wrong with eid {eid}; file {data_name}'.format(eid=eid,
                #                                                                              data_name=data_name))
                errors.append('Something went wrong with eid {eid}; file {data_name}'.format(eid=eid,
                                                                                             data_name=data_name))
        # for i in success: print(i)
        # for i in errors: print(i)


def extract_cards(cards):
    """ Card extraction. """
    # This function is stupidly the same as the one for blog entries

    for c in cards:
        global card_keys
        card = dict(zip(card_keys, c))
        template = templates[card['content_format']] # should I write another template just for card?

        card['etype'] = "Card"
        appendix = card['content_format'].split('/')[1]
        extension = 'rst' if appendix == 'rest' else appendix
        creation_date = datetime.strptime(card['creation_date'], '%Y/%m/%d %H:%M:%S')

        if card['category'] is None:
            card['category'] = 'none'

        if card['title']:
            card['underline'] = '#' * len(card['title'])
            card['slug'] = slugify(card['title']).lower()
        else:
            card['underline'] = '#' * len(str(card['eid']))
            card['slug'] = card['eid']

        write_card(card, template, extension)


def main():
    parser = argparse.ArgumentParser(description='Logilab extraction system.')
    parser.add_argument('-b', '--blogentries', action="store_true", help='extract blog entries & redirects?')
    parser.add_argument('-c', '--cards', action="store_true", help='extract cards?')
    parser.add_argument('-i', '--images', action="store_true", help='extract images?')

    # create images folders if not exists
    if not os.path.exists('./images/'):
        try:
            os.makedirs('./images/')
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    # print help message if no arguments passed
    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()

    if args.blogentries:
        print_warning("Extracting blogentries")
        blogentries = client.rql(blog_entries_query).json()
        extract_blog_entries(blogentries)
        update_articles()

    if args.cards:
        print_warning("Extracting cards")
        cards = client.rql(cards_query).json()
        extract_cards(cards)

    if args.images:
        print_warning("Extracting images")
        resp = client.rql(images_query)
        extract_images(resp)


if __name__ == "__main__":
    main()
