#!/usr/bin/env python3
# coding: utf8

import os
from cwclientlib import cwproxy


# BLOG ENTRIES EXTRACTION

template_rest = '''
{title}
{underline}


:slug: {slug}
:date: {creation_date}
:tags: {etype}
:category: {category}

{content}

'''

template_html = '''
<html>
    <head>
        <title>{title}</title>
        <meta name="slug" content="{slug}" />
        <meta name="date" content="{creation_date}" />
        <meta name="tags" content="{etype}" />
        <meta name="category" content="{category}" />
    </head>
    <body>
{content}
    </body>
</html>

'''


templates = {
    'text/html': template_html,
    'text/rest': template_rest
}

article_path = 'all_articles/{}/{}-{}.{}'
redirect_string_one = "Redirect /{etype}/{eid} /{year}/{month}/{slug}\n"
redirect_string_two = "Redirect /{eid} /{year}/{month}/{slug}\n"

blog_entries_query = (
    "Any BE, CAT, BET, CONT, DAT, FMT "
    "WHERE X is Blog, X title CAT, "
    "BE entry_of X, "
    "BE content CONT, BE content_format FMT, BE creation_date DAT, BE title BET")


client = cwproxy.CWProxy('https://www.logilab.fr/')
blog_keys = ['eid', 'category', 'title', 'content', 'creation_date', 'content_format']


# IMAGES EXTRACTION

image_url = os.environ.get('IMAGE_URL', 'https://www.logilab.fr/file/{eid}/raw/{data_name}')
image_path = os.environ.get('IMAGE_PATH', './images/{data_name}')

images_query = (
    'Any F, N, U '
    'where F is File, '
    'F data_name N, '
    'F cwuri U, '
    'F data_format in ("image/jpeg", "image/png")')


# CARDS EXTRACTION

cards_query = (
    "Any C, CAT, T, N, D, FMT "
    "WHERE F is Folder, " # use Folder or Theme as Category?
    "C filed_under F?, "
    "C title T, "
    "C content N, "
    "C creation_date D, "
    "C content_format FMT, "
    "F name CAT "
)

card_keys = ['eid', 'category', 'title', 'content', 'creation_date', 'content_format']
