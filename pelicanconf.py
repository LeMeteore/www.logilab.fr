#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Logilab'
SITENAME = 'Logilab'
SITEURL = ''

PATH = 'content'
THEME = 'themes/logilab'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (

('Thèmes', ''),

    ('Web Sémantique', ''),
    ('Informatique Scientifique', ''),
    ('Gestion de connaissances', ''),
    ('Outils et méthodes agiles', ''),
    ('Python', ''),
    ('Debian', ''),
    ('Salt', ''),
    ('Logiciel Libre', ''),

('Logilab', ''),

    ('Société', ''),
    ('Compétences', ''),
    ('Formations', ''),
    ('Emplois et Stages', ''),
    ('Contact', ''),

('Nous suivre', ''),

    ('Actualités', ''),
    ('Blogs / RSS', ''),
    ('Twitter', ''),
    ('LinkedIn', ''),
    ('Jabber', ''),

('Nos sites internet', ''),

    ('simulagora.com','https://www.simulagora.com'),
    ('logilab.org', 'https://www.logilab.org'),
    ('cubicweb.org', 'https://www.cubicweb.org'),
    ('semweb.pro', 'https://www.semweb.pro'),
)

ADDRESS = '''
    Paris : 104 Bd Blanqui 75013 Paris<br/>
    Toulouse : 1 Av de l'Europe 31400 Toulouse<br/>
    Nantes<br/>
    Valence
'''
MAIL = 'contact@logialb.fr'
PHONE = '+33.1.45.32.03.12'

TWITTER_USER = 'logilab'
LINKEDIN_USER = 'logilab'
MASTODON_USER = 'https://social.logilab.org/@logilab'

DEFAULT_PAGINATION = 10

# use the file system timestamp (mtime) can’t get date information from the metadata
# http://docs.getpelican.com/en/3.6.3/settings.html#basic-settings
DEFAULT_DATE = 'fs'

USE_FOLDER_AS_CATEGORY = True
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}/'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}/index.html'

# STATIC_PATHS = [
#     'content/admin/config.yml',
#     ]

TEMPLATE_PAGES = {'admin/index.html': 'admin/index.html'}
STATIC_PATHS = ['uploads', 'admin']

CMS_ENV = "development"

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
