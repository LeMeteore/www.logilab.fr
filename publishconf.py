#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# See: http://docs.getpelican.com/en/3.6.3/settings.html#basic-settings
SITEURL = os.environ.get('SITEURL', 'https://angry-hermann-f853e1.netlify.com')

RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
#CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

CMS_ENV = "production"
#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
