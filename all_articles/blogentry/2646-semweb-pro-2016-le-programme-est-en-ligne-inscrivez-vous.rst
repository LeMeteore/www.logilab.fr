
SemWeb.Pro 2016 : le programme est en ligne, inscrivez-vous !
#############################################################


:slug: semweb-pro-2016-le-programme-est-en-ligne-inscrivez-vous
:date: 2016/09/22 12:27:12
:tags: BlogEntry

.. image :: https://www.logilab.fr/file/2645/raw


**Lundi 21 novembre au FIAP Jean Monnet, à Paris**

Consultez le `programme et inscrivez-vous <//semweb.pro/semwebpro-2016.html>`_ dès à présent afin de
bénéficier du tarif à 65€ (passage à 100€ après le 4 novembre). 

*Vous pouvez assister à cette journée dans le cadre d'une
formation professionnelle (donnant lieu à l'établissement d'une
convention de formation). Dans ce cas, le tarif applicable est de
200€.*

Suivez nos actualités sur Twitter `@semwebpro <https://twitter.com/semwebpro>`_ mais aussi avec le hashtag `#semwebpro <https://twitter.com/hashtag/Semwebpro>`_

Pour plus d'informations, contactez-nous : contact@semweb.pro

