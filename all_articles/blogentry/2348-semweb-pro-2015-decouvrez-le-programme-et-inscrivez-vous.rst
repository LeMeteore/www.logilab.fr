
SemWeb.Pro 2015 : découvrez le programme et inscrivez-vous !
############################################################


:slug: semweb-pro-2015-decouvrez-le-programme-et-inscrivez-vous
:date: 2015/09/21 14:17:09
:tags: BlogEntry

`SemWeb.Pro 2015 <//semweb.pro/semwebpro-2015.html>`_ aura lieu jeudi 5 novembre au `FIAP Jean Monnet <//www.fiap.asso.fr/index.html>`_, à Paris.

À l'occasion de cette 4ème édition, nous souhaitons mettre l'accent sur l'utilisation des technologies du Web Sémantique dans un contexte professionnel.

Découvrez le `programme <//semweb.pro/semwebpro-2015.html#programme>`_ et `inscrivez-vous <//semweb.pro/semwebpro-2015.html#inscriptions>`_ dès à présent afin de garantir votre place !

Pour plus d'informations, contactez-nous : contact@semweb.pro

