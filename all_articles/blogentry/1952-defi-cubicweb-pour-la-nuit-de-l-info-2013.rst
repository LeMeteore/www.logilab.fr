
Défi CubicWeb pour la Nuit de l'info 2013
#########################################


:slug: defi-cubicweb-pour-la-nuit-de-l-info-2013
:date: 2013/12/03 11:47:50
:tags: BlogEntry

Logilab, en tant que partenaire de la `Nuit de l'info 2013 <//www.nuitdelinfo.com/>`_ qui aura lieu le 5 décembre 2013 `partout en France <//www.nuitdelinfo.com/nuitinfo/sites:start>`_, propose un `défi lié à CubicWeb <//www.nuitdelinfo.com/nuitinfo/defis:cubicweb:start>`_ et au web des données. Nous resterons en ligne jusqu'à minuit pour soutenir les participants !

.. image:: //www.logilab.fr/file/1953/raw/nuitinfo.png
   :align: center

