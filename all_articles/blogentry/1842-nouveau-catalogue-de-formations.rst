
Nouveau catalogue de formations
###############################


:slug: nouveau-catalogue-de-formations
:date: 2012/10/08 17:10:21
:tags: BlogEntry

Le nouveau `catalogue de formations`_ est en ligne !

Nouvelle organisation, améliorations de formations existantes, mais surtout nouvelles formations sur des thèmes tels que REST ou Cython. Consultez le `en ligne`_ ou `télécharger le PDF`_.

.. _`catalogue de formations`: //www.logilab.fr/formations
.. _`en ligne`: //www.logilab.fr/formations
.. _`télécharger le PDF`: //www.logilab.fr/publications/catalogue-formations.pdf

