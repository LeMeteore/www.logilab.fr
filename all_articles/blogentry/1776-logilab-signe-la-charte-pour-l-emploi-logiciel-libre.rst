
Logilab signe la Charte pour l'emploi logiciel libre
####################################################


:slug: logilab-signe-la-charte-pour-l-emploi-logiciel-libre
:date: 2012/09/27 11:21:27
:tags: BlogEntry

La charte vise à garantir des bonnes pratiques en matière d’emploi dans le secteur des logiciels libres: Environnement de travail libre, condition d’emploi favorable, soutien aux formations dédiées à ces technologies. 

TODO - en attente de la publication du texte par les orga ayant initié le truc.

Initié par le think tank `Education Job & FLOSS <//www.educationjobandfloss.org/>`_, la charte est promue par le `CNLL (Conseil National du Logiciel Libre) <//www.cnll.fr/>`_, `PLOSS (le réseau des Entreprises du Logiciel Libre en Île-de-France  <//ploss.hosting.enovance.com/>`_

