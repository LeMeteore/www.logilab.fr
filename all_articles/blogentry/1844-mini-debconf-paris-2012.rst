
Mini DebConf Paris 2012
#######################


:slug: mini-debconf-paris-2012
:date: 2012/10/25 13:29:34
:tags: BlogEntry

Logilab a le plaisir de soutenir financièrement la `mini-conférence Debian`__ qui aura lieu le 24 et 25 novembre 2012 à Paris. Venez-y nombreux !

__ //fr2012.mini.debconf.org/

