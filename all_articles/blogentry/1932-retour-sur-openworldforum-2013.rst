
Retour sur OpenWorldForum 2013
##############################


:slug: retour-sur-openworldforum-2013
:date: 2013/10/08 17:58:23
:tags: BlogEntry

Merci à tous les participants de l'`Open World Forum 2013 <//openworldforum.org/>`_ pour les nombreux échanges de qualité que nous avons pu avoir pendant les deux premières journées. Retrouvez `ici la présentation <//www.logilab.fr/file/1931/raw/presentation.pdf>`_ (miroir sur `slideshare <//www.slideshare.net/logilab/prsentation-sur-le-stand-lopenworldforum-2013>`_) présentée sur notre stand. Une mention particulière pour `Thomas Hatch <https://github.com/thatch45>`_ de `SaltStack <//saltstack.com/>`_ qui a animé avec brio le meetup que nous organisions.


.. image :: //openworldforum.org/static/pictures/Calque1.png
  :align: center

