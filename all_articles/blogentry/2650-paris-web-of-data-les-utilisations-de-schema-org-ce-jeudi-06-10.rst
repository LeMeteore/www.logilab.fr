
Paris Web of Data - "les utilisations de schema.org" ce jeudi 06/10
###################################################################


:slug: paris-web-of-data-les-utilisations-de-schema-org-ce-jeudi-06-10
:date: 2016/09/29 09:41:18
:tags: BlogEntry

Pour rappel, la prochaine rencontre du groupe **Paris Web of Data : les rencontres du Web de données** aura lieu la semaine prochaine, **jeudi 6 octobre de 19h00 à 22h00** dans les locaux de Google.


.. image :: https://www.logilab.fr/file/2649/raw

Des orateurs présenteront leur utilisation de schema.org dans diverses applications professionnelles :

- SEO sémantique (search engine optimization grâce à schema.org)
- Google Knowledge Graph (exemple de l'indexation de YouTube)
- Évolution et extension de schema.org
- utilisation de schema.org dans le domaine du tourisme

`Inscription gratuite <//www.meetup.com/fr-FR/paris-web-of-data/events/232635663/#event-comments-section>`_, mais obligatoire !

