
AG actionnaires de Logilab - 2013
#################################


:slug: ag-actionnaires-de-logilab-2013
:date: 2013/10/24 18:45:49
:tags: BlogEntry

Les actionnaires de Logilab se réuniront en assemblée générale le 7 novembre 2013 pour examiner l'exercice 2012-2013.

Malgré un contexte difficile au premier semestre 2013, la société termine bénéficiaire et les perspectives restent bonnes pour 2014.

.. image :: /data/logo.png

