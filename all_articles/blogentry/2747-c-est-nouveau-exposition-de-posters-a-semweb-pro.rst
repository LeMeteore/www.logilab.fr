
C'est nouveau : exposition de posters à SemWeb.Pro
##################################################


:slug: c-est-nouveau-exposition-de-posters-a-semweb-pro
:date: 2017/10/27 18:49:14
:tags: BlogEntry

.. image :: https://www.logilab.fr/file/2748/raw


Pour cette sixième édition de SemWeb.Pro, des posters seront exposés dans le hall de la conférence.
Rendez-vous mercredi 22 novembre
au FIAP Jean Monnet, à Paris.

Participer à SemWeb.Pro c'est l'occasion d'échanger avec les membres de la communauté du Web Sémantique ainsi qu'avec des utilisateurs, issus de l'industrie ou de la culture, qui mettent en œuvre les nouvelles techniques du Web des données.

Consultez le `programme <//www.semweb.pro/semwebpro-2017.html#programme et inscrivez-vous !>`_ et `inscrivez-vous <//www.semweb.pro/semwebpro-2017.html#inscriptions>`_

Twitter @semwebpro #semwebpro

Pour plus d'informations, contactez-nous : contact@semweb.pro

