
Logilab vous attend sur son stand à la Convention Systematic.
#############################################################


:slug: logilab-vous-attend-sur-son-stand-a-la-convention-systematic
:date: 2016/05/23 14:51:48
:tags: BlogEntry

La 11ème édition de la Convention Systematic se tiendra mercredi 8 juin à la Maison de la Chimie, à Paris. Cette édition valorisera les différentes technologies clefs du Pôle et de ses membres. 

Logilab, membre très actif du Pôle Systematic sur la thématique du *Logiciel Libre*\ , sera présente sur l'espace d'exposition, de 12h30 à 17h00, pour vous dévoiler ses produits et services innovants !

.. image :: https://www.logilab.fr/file/2607/raw  

Découvrez le `programme <//www.events-systematic-paris-region.org/programme/>`_ de la prochaine **convention du Pôle Systematic qui aura lieu mercredi 8 juin à la Maison de la Chimie, à Paris.**

Entrée gratuite, mais inscription impérative !

