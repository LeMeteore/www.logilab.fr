
Soirée Python à Nantes le 18 mai 2016
#####################################


:slug: soiree-python-a-nantes-le-18-mai-2016
:date: 2016/04/29 15:00:07
:tags: BlogEntry

Logilab est co-organisateur du **meet-up Python qui aura lieu mercredi 18 mai, à Nantes.**

Les deux présentations de la soirée :

* Dejan Filipovic de Gandi parlera de `Celery <//www.celeryproject.org/>`_;

* `Arthur Lutz <https://twitter.com/arthurlutz>`_ de Logilab, présentera un retour d'expérience sur l'utilisation du collecteur d'erreur multi-langage `Sentry <https://github.com/getsentry/sentry>`_.

.. image :: https://www.logilab.fr/file/2572/raw

Nous vous donnons rendez-vous à 19h à la `Cantine <//cantine.atlantic2.org/>`_ située au 11 impasse Juton, à Nantes.

**Entrée gratuite, mais inscription obligatoire.**
`INSCRIVEZ-VOUS ! <//www.meetup.com/fr-FR/Nantes-Python-Meetup/events/230661649/?eventId=230661649>`_

