
Capitole du Libre édition 2015 annulée
######################################


:slug: capitole-du-libre-edition-2015-annulee
:date: 2015/11/17 12:48:21
:tags: BlogEntry

**Le Capitole du Libre est annulé**

Organisée par l'`association Toulibre <//toulibre.org/>`_  avec la participation active des clubs étudiants de l'N7, la prochaine édition de `Capitole du Libre <https://2015.capitoledulibre.org/>`_ a été annulée par l'INP Toulouse et la Préfecture de Police.

Suite aux attentats qui ont eu lieu vendredi 13 novembre, `aucun évènement public ne peut se tenir dans les locaux de l'INP-ENSEEIHT <https://twitter.com/capitoledulibre/status/666256882106040320>`_.

Nous attendons avec impatience l'édition 2016 pour nous retrouver avec la communauté du libre toulousaine.

