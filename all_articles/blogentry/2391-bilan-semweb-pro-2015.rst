
Bilan SemWeb.Pro 2015
#####################


:slug: bilan-semweb-pro-2015
:date: 2015/11/25 12:15:33
:tags: BlogEntry

.. image:: https://www.logilab.fr/file/2396?vid=download


Organisé par Logilab avec le soutien de l'`INRIA <//www.inria.fr/>`_, `SemWeb.Pro <//semweb.pro/>`_ a eu lieu le 5 novembre au FIAP Jean Monnet, à Paris.


`Découvrez le bilan de cette édition. <//semweb.pro/blogentry/510682>`_


.. image:: https://www.logilab.fr/file/2397?vid=download


Merci à toutes et à tous pour votre participation. Suivez notre fil `Twitter <https://twitter.com/semwebpro>`_ pour être informé de la date de l'appel à communication pour l'édition 2016 si vous souhaitez proposer un sujet ainsi que la date de la prochaine édition SemWeb.Pro.

