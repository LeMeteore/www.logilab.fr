
Convention Systematic 2014
##########################


:slug: convention-systematic-2014
:date: 2014/06/11 15:22:47
:tags: BlogEntry

Logilab participera à la convention du pôle de compétitivité Systematic qui aura lieu le 24 juin 2014 à Paris. `Simulagora <https://www.simulagora.com>`_ fait d'ailleurs partie des produits en lice pour le `prix de l'innovation <//www.events-systematic-paris-region.org/#!les-innovations/c23lb>`_ qui sera attribué à cette occasion.

Venez nous rendre visite sur notre stand pour une démo et pour nous parler des projets innovants que nous pourrions vous aider à réaliser !

.. image:: //www.logilab.fr/file/1410/raw/systematic.png

