
Logilab sera présent demain au Barcamp Open Data Toulouse métropole
###################################################################


:slug: logilab-sera-present-demain-au-barcamp-open-data-toulouse-metropole
:date: 2013/10/08 17:20:38
:tags: BlogEntry

Retrouvez nous demain à la cantine de Toulouse où plusieurs personnes de Logilab seront présentes pour participer au barcamp autour des données de la métropole toulousaine !

Plus d'infos sur `barcamp.org <//barcamp.org/w/page/68952350/Barcamp%20Open%20Data%20Toulouse%20M%C3%A9tropole>`_

