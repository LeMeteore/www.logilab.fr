
[Présentation annulée] Pourquoi et comment Logilab investit dans la R&D ?
#########################################################################


:slug: presentation-annulee-pourquoi-et-comment-logilab-investit-dans-la-r-d
:date: 2016/02/01 17:00:25
:tags: BlogEntry

La présentation `"Pourquoi et comment les entreprises open source investissent dans la R&D ?" <//www.digitalplace.fr/index.php/fr/evenement-innovation/item/pourquoi-et-comment-les-entreprises-open-source-investissent-dans-la-r-d>`_ prévue ce mercredi 3 février de 12h à 14h à Digital Place a été annulée par les organisateurs.

Nous vous communiquerons la nouvelle date dès que nous en aurons connaissance.

