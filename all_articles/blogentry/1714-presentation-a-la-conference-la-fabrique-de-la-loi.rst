
Présentation à la conférence "La Fabrique de la Loi"
####################################################


:slug: presentation-a-la-conference-la-fabrique-de-la-loi
:date: 2012/06/25 18:04:40
:tags: BlogEntry

Le 7 juillet 2012, Logilab participera à la conférence "La Fabrique de la Loi" organisée par `Regards Citoyens <//www.regardscitoyens.org/>`_ et le `Medialab de Science-Po Paris <//www.medialab.sciences-po.fr/>`_. Pour plus d'informations, consultez le site `//www.lafabriquedelaloi.fr/ <//www.lafabriquedelaloi.fr/les-partenaires/>`_. Les inscriptions sont ouvertes `ici <//www.lafabriquedelaloi.fr/conference/registration/>`_, le `programme complet  <//www.lafabriquedelaloi.fr/conference/programme/>`_ a été publié. Notre présentation a pour titre "Information Extraction from News Articles Using Open Datasets" et fera une démonstration de l'utilisation de `CubicWeb <//www.cubicweb.org>`_ pour extraire des informations structurées de coupures de presse.

