
Unlish, une application CubicWeb
################################


:slug: unlish-une-application-cubicweb
:date: 2015/09/03 11:47:03
:tags: BlogEntry

`Unlish <//www.unlish.com/>`_, le réseau social qui réunit tous les sportifs, cherchait un environnement de développement Python qui permettrait de réaliser un traitement sémantique des données produites par son service. Unlish a donc fait le choix d'utiliser `CubicWeb <https://www.cubicweb.org/>`_, pour sa qualité technologique ainsi que pour son intégration avec le « `web des données <https://www.logilab.fr/web-semantique>`_ ».


    "Un des points critiques dans le développement des applications Unlish est la gestion fine et complexe des permissions. L'accès aux données personnelles et aux événements privés est fondamentale dans la feuille de route de développement du produit. La technologie du moteur de données (RQL) et le système de permission développé par CubicWeb nous a permis d'assurer cette gestion des droits de manière aisée."

    -- Matthieu Pesin, PDG d'Unlish


Unlish s'implique dans la communauté CubicWeb et supporte son développement tout en contribuant à ce que le projet soit encore plus accessible à d'autres développeurs, notamment à l'intégration de CubicWeb avec Pyramid, en proposant de nouvelles approches dans le packaging ou l'exploitation des web services.

À noter que Christophe de Vienne d'Unlish sera présent à `PyConFr 2015 <//www.pycon.fr/2015/schedule/>`_, à Pau et parlera de l'insertion de CubicWeb dans l'environnement Pyramid.

Pour plus d'informations sur Unlish, rendez-vous sur //www.unlish.com ou téléchargez l'application sur votre smartphone.

