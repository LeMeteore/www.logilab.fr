
Logilab présent aux journées d'études "Documenter la production artistique : données, outils, usages"
#####################################################################################################


:slug: logilab-present-aux-journees-d-etudes-documenter-la-production-artistique-donnees-outils-usages
:date: 2018/06/05 16:20:53
:tags: BlogEntry

Du 4 au 6 juin ont lieu les journées d'études **Documenter la production artistique : données, outils, usages** *autour des plateformes ReSource et Artefactory* qui se déroulent à la `Villa Arson <//www.villa-arson.org>`_, à Nice.

À cette occasion, `Adrien di Mascio <https://twitter.com/adimasci>`_ présentera **Désilotation et publication de données culturelles : un retour d’expérience**.


Cette présentation expliquera de manière simple les notions d’échelle de qualité
des données, d’ontologies, de référentiels, d’identifiants pérennes et d’alignements. 
Adrien montrera quelques retours d’expériences concrets de projets réalisés par Logilab comme `data.bnf.fr <//data.bnf.fr/>`_, `francearchives.fr <https://francearchives.fr/>`_ ou `Biblissima <//www.biblissima-condorcet.fr/>`_ pour illustrer les différents concepts
et processus mis en jeu pour publier des données patrimoniales.

**Rendez-vous demain, mercredi 6 juin à partir de 10h45 à l'amphi 3 de la Villa Arson.**

