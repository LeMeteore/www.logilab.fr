
Logilab au Salon du Bourget et Forum Teratec 2015
#################################################


:slug: logilab-au-salon-du-bourget-et-forum-teratec-2015
:date: 2015/07/21 18:06:04
:tags: BlogEntry

Logilab a présenté `Simulagora : un service web de simulation numérique dans le cloud <//www.slideshare.net/logilab/simulagora-salon-du-bourget>`_ lors de la 51ème édition du `Salon International de l'Air et de l'Espace <//www.siae.fr>`_ au Bourget et lors de la 10ème édition du `Forum Teratec <//www.teratec.eu/gb/forum/index.html>`_, dédié à la simulation numérique et au calcul haute performance (HPC).

Visionnez la `vidéo de démonstration <https://www.youtube.com/watch?v=mEHLgwhtF40>`_.

