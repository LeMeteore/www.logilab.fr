
Paris Web of Data - les utilisations de schema.org le 6 octobre 2016
####################################################################


:slug: paris-web-of-data-les-utilisations-de-schema-org-le-6-octobre-2016
:date: 2016/09/09 10:43:32
:tags: BlogEntry

Nous vous invitons à participer à la prochaine session du groupe **Paris Web of Data : les rencontres du Web de données** qui aura lieu jeudi **6 octobre 2016** de **19h00 à 22h** chez **Google**.

À cette occasion, les différentes utilisations de `schema.org <//schema.org>`_ seront présentées:

- SEO sémantique (search engine optimization grâce à schema.org)
- Google Knowledge Graph (exemple de l'indexation de YouTube)
- évolution et extension de schema.org
- utilisation de schema.org dans le domaine du tourisme


Pour participer, il suffit de vous `inscrire <//www.meetup.com/fr-FR/paris-web-of-data/events/232635663/>`_ ! 

Attention ! Le nombre de places est limité !

