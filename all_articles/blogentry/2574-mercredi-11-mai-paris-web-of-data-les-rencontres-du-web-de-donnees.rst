
Mercredi 11 mai : Paris Web of Data, les rencontres du Web de données
#####################################################################


:slug: mercredi-11-mai-paris-web-of-data-les-rencontres-du-web-de-donnees
:date: 2016/05/02 16:19:41
:tags: BlogEntry

Nous vous invitons à participer au prochain meetup **Paris Web of Data : les rencontres du Web de données** qui aura lieu mercredi **11 mai** à **19h00** chez **Mozilla**.

À cette occasion, le projet **DOREMUS, DOing REusable MUSical data** sera présenté.

.. image :: https://www.logilab.fr/file/2576/raw

`Paris Web of Data <//www.meetup.com/fr-FR/paris-web-of-data/events/229605996/>`_ a été créé en 2001 par `Alexandre Monnin <https://twitter.com/aamonnz>`_. Le groupe  compte aujourd'hui plus de 600 membres. Une nouvelle équipe prend en main son animation : `Nicolas Chauvat <https://twitter.com/nchauvat>`_, PDG de Logilab, `Marie Destandau <https://twitter.com/marie_ototoi>`_, développeur front-end / chargée de recherche à la Philharmonie de Paris et `Jean Delahousse <https://twitter.com/jdelahousse>`_, expert des technologie du web sémantique.

Pour participer, il suffit de vous `inscrire <//www.meetup.com/fr-FR/paris-web-of-data/events/229605996/>`_ ! 

Attention ! Le nombre de places est limité !

