
Réunion Salt le 19 mai 2014 à Paris
###################################


:slug: reunion-salt-le-19-mai-2014-a-paris
:date: 2014/05/14 11:40:01
:tags: BlogEntry

Logilab organisera le 19 mai 2014 à Paris à partir de 19h, dans les locaux de l'IRILL_, la quatrième réunion des utilisateurs et développeurs de Salt_ en France. Salt est un environnement d'exécution distribué et asynchrone, écrit en Python, qui se positionne comme le couteau suisse de la gestion d'infrastructure.

`Thomas Hatch <https://twitter.com/thatch45>`_, le concepteur de Salt et fondateur de SaltStack_, sera présent lors de cette réunion, à laquelle il participera après sa présentation à la conférence dotScale_.

L'IRILL est au 23 avenue d'Italie à Paris. L'entrée sera gratuite et sans réservation, mais il est recommandé de s'annoncer sur //framadate.org/vfmrfutv3eyudg5b

Logilab, qui a choisi Salt pour la gestion de sa propre infrastructure (interne multi-site et `calcul / simulagora <//www.simulagora.com>`_), participe au développement et propose du `conseil et de la formation <//www.logilab.fr/training/1942>`_ pour le maîtriser rapidement.

.. _IRILL: //www.irill.org/
.. _Salt: //www.saltstack.com/community/
.. _SaltStack: //www.saltstack.com/
.. _dotScale: //www.dotscale.eu/

.. image:: //www.logilab.fr/file/2163/raw/saltstack_logo.png
   :width: 400px

