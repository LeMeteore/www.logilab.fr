
Réunion Salt le 15 avril 2014 à Paris
#####################################


:slug: reunion-salt-le-15-avril-2014-a-paris
:date: 2014/04/01 12:06:32
:tags: BlogEntry

Logilab organisera le 15 avril 2014 à Paris à partir de 19h, dans les locaux de l'IRILL_, la troisième réunion des utilisateurs et développeurs de Salt_ en France. Salt est un environnement d'exécution distribué et asynchrone, écrit en Python, qui se positionne comme le couteau suisse de la gestion d'infrastructure.

L'IRILL est au 23 avenue d'Italie à Paris. L'entrée sera gratuite et sans réservation, mais il est recommandé de s'annoncer sur la liste //lists.afpy.org/listinfo/salt-fr

Logilab, qui a choisi Salt pour la gestion de sa propre infrastructure (interne multi-site et `calcul / simulagora <//www.simulagora.com>`_), participe au développement et propose du `conseil et de la formation <//www.logilab.fr/training/1942>`_ pour le maîtriser rapidement.

.. _IRILL: //www.irill.org/
.. _Salt: //www.saltstack.com/community/

.. image:: //www.logilab.fr/file/2163/raw/saltstack_logo.png
   :width: 400px

