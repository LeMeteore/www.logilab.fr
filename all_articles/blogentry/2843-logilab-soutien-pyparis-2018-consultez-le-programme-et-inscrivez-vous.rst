
Logilab soutien PyParis 2018 : consultez le programme et inscrivez-vous !
#########################################################################


:slug: logilab-soutien-pyparis-2018-consultez-le-programme-et-inscrivez-vous
:date: 2018/10/30 12:15:33
:tags: BlogEntry

Depuis son lancement, Logilab soutien PyParis : deux jours de conférence qui réuni des utilisateurs et des développeurs du langage de programmation Python.

.. image :: https://www.logilab.fr/file/2842/raw/PyParis.png/raw

**À cette occasion,** `Arthur Lutz <https://twitter.com/arthurlutz>`_  **présentera "Python tooling for continuous deployment"**. 
Il expliquera comment au sein de `Logilab <https://www.logilab.fr/>`_ nous avons migré les processus de génération et de déploiement vers un modèle de distribution continue, les conséquences d'un tel changement en termes de technologie, au sein des équipes et de gestion de projet avec les clients. Cette présentation portera sur les outils Python qui ont permis de réaliser un tel changement, mais également sur les changements humains qu’il nécessite.

Consultez le `programme <//pyparis.org/talks.html>`_ et `inscrivez-vous <https://www.weezevent.com/pyparis-2018>`_ !

