
Pourquoi et comment Logilab investit dans la R&D ?
##################################################


:slug: pourquoi-et-comment-logilab-investit-dans-la-r-d
:date: 2016/01/19 17:49:45
:tags: BlogEntry

Logilab, avec d'autres PME membres de l'association `SoLibre <//www.solibre.fr/fr/index.html>`_, présentera à `Digital Place <//www.digitalplace.fr>`_ les raisons de son investissement en R&D et ses projets de R&D collaboratifs.

**Rendez-vous mercredi 3 février de 12h à 14h à Digital Place**

.. image :: https://www.logilab.fr/file/2439?vid=download


Accédez au `programme <//www.digitalplace.fr/index.php/fr/evenement-innovation/item/pourquoi-et-comment-les-entreprises-open-source-investissent-dans-la-r-d>`_ et `inscrivez-vous <https://docs.google.com/forms/d/1gd9KDwC144kn5hL-Q64l3E7btxqppnftP9zFuSM7ShA/viewform?c=0&w=1>`_ !

