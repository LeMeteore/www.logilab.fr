
Nouvelle formation "Apprentissage statistique et fouille de données avec Python"
################################################################################


:slug: nouvelle-formation-apprentissage-statistique-et-fouille-de-donnees-avec-python
:date: 2013/07/24 09:12:02
:tags: BlogEntry

Une `nouvelle formation`_ à destination des personnes souhaitant utiliser Python pour faire de l'apprentisage statistique et de la fouille de données (*Machine learning*) vient d'être ajoutée à notre catalogue. Elle présente l'état de l'art du sujet et les bibliothèques tierces rendant cela accessible, telles que `scikit.learn`_.

Cette nouvelle formation d'une durée de trois jours demande une connaissance basique du langage de programmation Python. Elle sera disponible en inter-entreprises comme en intra-entreprise.

.. _`nouvelle formation`: //www.logilab.fr/formations/python-learn
.. _`scikit.learn`: //scikit-learn.org/

