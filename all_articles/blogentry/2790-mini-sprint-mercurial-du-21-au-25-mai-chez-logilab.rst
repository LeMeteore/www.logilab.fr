
Mini-sprint mercurial du 21 au 25 mai chez Logilab
##################################################


:slug: mini-sprint-mercurial-du-21-au-25-mai-chez-logilab
:date: 2018/05/04 18:16:19
:tags: BlogEntry

Logilab co-organise avec la société `Octobus <https://octobus.net/>`_, un mini-sprint Mercurial qui aura lieu du 21 au 25 mai au sein des `locaux logilabiens <https://www.logilab.fr/contact>`_, à Paris.

.. image :: https://www.logilab.fr/file/2791/raw

Afin d'y participer, remplissez le sondage ci-dessous en nous indiquant votre nom et les dates de votre choix.

https://framadate.org/sprint-hg

Nous vous invitons également à remplir le pad et nous indiquer les thématiques que vous souhaitez aborder au cours de ce sprint : https://mensuel.framapad.org/p/mini-sprint-hg

**Let's code together!**

