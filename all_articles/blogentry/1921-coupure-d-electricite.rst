
Coupure d'électricité
#####################


:slug: coupure-d-electricite
:date: 2013/07/26 18:02:00
:tags: BlogEntry

Une coupure d'électricité a touché l'ensemble de notre quartier pendant près de 6 heures aujourd'hui. Nous avons donc dû interrompre un certain nombre de nos sites et services Web. Actuellement, ils fonctionnent à nouveau mais sur une alimentation de secours :

.. image:: //www.logilab.fr/file/1920/raw/groupe_electrogene_erdf.jpg
   :width: 500px

Nous espérons un retour à la normale pour le début de la semaine prochaine.

