
Logilab, un industriel du logiciel
##################################


:slug: logilab-un-industriel-du-logiciel
:date: 2016/03/22 17:54:51
:tags: BlogEntry

À l'occasion de la Semaine de l'Industrie, nous avons participé à une présentation des PME et du riche vivier d'emplois qu'elles offrent. Cette demi-journée, organisée par `Pôle Emploi Paris 15ème <//www.pole-emploi.fr/>`_  et `Systematic <//www.systematic-paris-region.org/>`_, a permis d'ouvrir de nouveaux horizons à un public attentif de chercheurs d'emploi.

`Olivier Cayrol <//www.twitter.com/OCayrol>`_ a illustré la conférence en présentant la société Logilab et les `postes </emplois>`_ qu'elle cherche à pourvoir.

.. image :: https://www.logilab.fr/file/2551?vid=download

`Découvrez sa présentation et tout notre savoir-faire ! <//slides.logilab.fr/2016/semaine_industrie/logilab.pdf>`_ Vous pouvez également visualisez cette présentation sur `slideshare <//fr.slideshare.net/logilab/prsentation-logilab>`_.

