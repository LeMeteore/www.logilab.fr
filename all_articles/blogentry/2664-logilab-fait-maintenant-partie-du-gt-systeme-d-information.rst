
Logilab fait maintenant partie du GT Système d'Information
##########################################################


:slug: logilab-fait-maintenant-partie-du-gt-systeme-d-information
:date: 2016/10/10 16:42:52
:tags: BlogEntry

Début octobre, `Olivier Cayrol <https://twitter.com/OCayrol>`_ a présenté la société Logilab et son savoir-faire au comité de pilotage du `Groupe thématique Systèmes d'Information <//www.systematic-paris-region.org/fr/nos-thematiques>`_ du pôle Systematic Paris Région.

Nous sommes très heureux de faire aujourd'hui parti de ce groupe et avons hâte de participer à des projets de R&D collaboratifs qui mettront au point des outils innovants de gestion d'entreprise.

.. image :: https://www.logilab.fr/file/2665/raw

