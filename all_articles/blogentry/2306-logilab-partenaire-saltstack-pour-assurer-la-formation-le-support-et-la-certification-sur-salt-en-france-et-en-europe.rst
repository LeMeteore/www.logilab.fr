
Logilab partenaire SaltStack pour assurer la formation, le support et la certification sur Salt en France et en Europe
######################################################################################################################


:slug: logilab-partenaire-saltstack-pour-assurer-la-formation-le-support-et-la-certification-sur-salt-en-france-et-en-europe
:date: 2015/04/17 17:38:43
:tags: BlogEntry

Logilab a annoncé hier lors du meetup Salt son partenariat avec SaltStack pour assurer le support, la formation et la certification pour les logiciels SaltStack Enterprise en France et en Europe. 

La prochaine session de formation Salt se tiendra du mercredi 10 au vendredi 12 juin dans les locaux de Logilab.

Lire le `communiqué de presse`_.

.. _`communiqué de presse`: //www.logilab.fr/file/2307/raw/CP_partenariat_SaltStack.pdf

