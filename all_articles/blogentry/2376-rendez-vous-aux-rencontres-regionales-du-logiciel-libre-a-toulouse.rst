
Rendez-vous aux Rencontres Régionales du Logiciel Libre à Toulouse
##################################################################


:slug: rendez-vous-aux-rencontres-regionales-du-logiciel-libre-a-toulouse
:date: 2015/11/02 17:45:23
:tags: BlogEntry

Acteur de l'association `SoLibre <//solibre.fr/>`_, qui fédère les professionnels de l'Open Source dans le Sud-Ouest, Logilab vous invite à nous retrouver à la 3ème édition des Rencontres Régionales du Logiciel Libre  - RRLL - qui aura lieu jeudi 3 décembre à l'Hippodrome de Toulouse.

Consultez le `programme <//www.solibre.fr/fr/actualites/rejoignez-nous-aux-rrll-2015.html>`_ et `rejoignez-nous <https://www.eventbrite.fr/e/billets-rencontres-regionales-du-logiciel-libre-2015-19100604422>`_ !

