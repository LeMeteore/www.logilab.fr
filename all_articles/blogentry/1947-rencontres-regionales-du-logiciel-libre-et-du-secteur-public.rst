
Rencontres Régionales du Logiciel Libre et du Secteur Public
############################################################


:slug: rencontres-regionales-du-logiciel-libre-et-du-secteur-public
:date: 2013/11/05 17:09:12
:tags: BlogEntry

À l’initiative du CNLL_ et des représentants régionaux, les `rencontres régionales du Logiciel Libre`_ ont pour ambition de mettre en relation les professionnels du libre et les acteurs publics.

Dans ce cadre Logilab_, via l’association SoLibre_,
participe à l’organisation de l'`étape Toulousaine`_ le 22 novembre. Nous serons également présent à l'`étape Bordelaise`_ du tour le 13 novembre prochain.

Plus d'information sur le programme de Toulouse sur le `site de SoLibre`_ et sur le programme de Bordeaux sur le `site d'Acquinetic`_.

.. image:: //www.rrll.fr/images/carte-france2.png
  :align: center

.. _CNLL: //www.cnll.fr/
.. _`rencontres régionales du Logiciel Libre`: //www.rrll.fr/
.. _Logilab: //www.logilab.fr
.. _SoLibre: //www.solibre.fr/
.. _`étape Toulousaine`: //www.solibre.fr/fr/les-rencontres-regionales-du-logiciel-libre.html
.. _`étape Bordelaise`: //www.pole-aquinetic.fr/fr/content/13-novembre-2013-bordeaux-rencontres-r%C3%A9gionales-du-logiciel-libre-hotel-de-r%C3%A9gion
.. _`site de SoLibre`: //www.solibre.fr/fr/les-rencontres-regionales-du-logiciel-libre.html
.. _`site d'Acquinetic`: //www.pole-aquinetic.fr/fr/content/13-novembre-2013-bordeaux-rencontres-r%C3%A9gionales-du-logiciel-libre-hotel-de-r%C3%A9gion

