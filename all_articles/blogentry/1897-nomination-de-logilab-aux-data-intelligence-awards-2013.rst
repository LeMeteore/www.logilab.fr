
Nomination de Logilab aux Data Intelligence Awards 2013
#######################################################


:slug: nomination-de-logilab-aux-data-intelligence-awards-2013
:date: 2013/03/14 22:03:31
:tags: BlogEntry

Logilab participera jeudi prochain à la `finale du concours Data Intelligence Awards 2013 <//www.veillemag.com/Nomines-aux-Data-Intelligence-Awards-2013-Documation-MIS_a2054.html>`_, organisé au salon Documation, en présentant le projet OpenCat, co-financé par la Bibliothèque nationale de France, le Ministère de la Culture et de la Communication.

La vidéo et le support de présentation seront bientôt mis en ligne.

