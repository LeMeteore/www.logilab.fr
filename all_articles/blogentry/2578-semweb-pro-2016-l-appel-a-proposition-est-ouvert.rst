
SemWeb.Pro 2016 : l'appel à proposition est ouvert !
####################################################


:slug: semweb-pro-2016-l-appel-a-proposition-est-ouvert
:date: 2016/05/09 17:24:21
:tags: BlogEntry

Organisée par Logilab, la cinquième édition de **SemWeb.Pro aura lieu lundi 21 novembre 2016 au FIAP Jean Monnet, à Paris.**

.. image:: https://www.logilab.fr/file/2396?vid=download



**Appel à propositions**


Pour soumettre au comité de programme votre proposition de présentation, nous vous invitons à envoyer un courrier électronique à programme@semweb.pro avant le vendredi 8 juillet 2016 en précisant les informations suivantes :

- titre,
- description en moins de 400 mots
- auteur présenté en quelques phrases

Pour toute demande d'informations, consultez le site `SemWeb.Pro 2016 <//semweb.pro/semwebpro-2016.html>`_ ou envoyez un courrier électronique à contact@semweb.pro.


Suivez nos actualités sur Twitter `@semwebpro <https://twitter.com/semwebpro>`_ mais aussi avec le hashtag `#semwebpro <https://twitter.com/hashtag/semwebpro>`_

