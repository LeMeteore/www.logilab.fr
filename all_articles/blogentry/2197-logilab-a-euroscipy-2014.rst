
Logilab à EuroSciPy 2014
########################


:slug: logilab-a-euroscipy-2014
:date: 2014/09/03 12:12:31
:tags: BlogEntry

Logilab a participé à EuroSciPy 2014, la conférence européenne des utilisateurs de Python dans le domaine scientifique, en présentant deux posters sur `PAFI <//www.logilab.org/file/264587/raw/pafi.pdf>`_ et `Simulagora <//www.logilab.org/file/264589/raw/reproducibility.pdf>`_.

Notre `compte-rendu <//www.logilab.org/blogentry/264586>`_ détaille quelques présentations phares de la conférence. Merci aux organisateurs et rendez-vous de nouveau à Cambridge l'année prochaine !

.. image:: //www.euroscipy.org/2014/site_media/static/symposion/img/logo.png
   :align: center

