
Lancement du blog Simulagora
############################


:slug: lancement-du-blog-simulagora
:date: 2015/09/02 11:44:02
:tags: BlogEntry

Retrouvez sur ce blog tous les billets écrits sur `Simulagora <https://www.simulagora.com>`_, la plateforme de simulation numérique en ligne, depuis son lancement en 2012. Découvrez les dernières actualités et les prochains évènements autour de Simulagora.

Rendez-vous sur //blog.simulagora.com/

