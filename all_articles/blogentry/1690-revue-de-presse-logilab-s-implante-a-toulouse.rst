
Revue de presse : Logilab s'implante à Toulouse
###############################################


:slug: revue-de-presse-logilab-s-implante-a-toulouse
:date: 2012/05/14 09:50:27
:tags: BlogEntry

`Midi Pyrénées Expansion`_, l'agence régionale qui aide Logilab à se développer sur Toulouse, publie un article_ sur notre implantation.
Merci à l'équipe et particulièrement à Vincent Vigié pour son accompagnement !

.. image:: //www.logilab.fr/file/1691?vid=download

.. _`Midi Pyrénées Expansion`: //www.midipyrenees-expansion.fr/
.. _article: //www.midipyrenees-expansion.fr/fiches/fiche_breve_4.php?id=500

