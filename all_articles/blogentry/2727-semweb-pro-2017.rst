
SemWeb.Pro 2017
###############


:slug: semweb-pro-2017
:date: 2017/09/19 16:41:56
:tags: BlogEntry

.. image :: https://www.logilab.fr/file/2645/raw




Nous vous donnons rendez-vous pour la 6ème édition de SemWeb.Pro, journée de présentations et de rencontres dédiées au web sémantique dans le monde professionnel.

**Mercredi 22 novembre au FIAP Jean Monnet, à Paris**

Consultez le `programme et inscrivez-vous <//semweb.pro/semwebpro-2017.html>`_ dès à présent afin de
bénéficier du tarif à 67€ (passage à 100€ après le 13 octobre). 

*Vous pouvez assister à cette journée dans le cadre d'une
formation professionnelle (donnant lieu à l'établissement d'une
convention de formation). Dans ce cas, le tarif applicable est de
200€.*

Suivez nos actualités sur Twitter `@semwebpro <https://twitter.com/semwebpro>`_ mais aussi avec le hashtag `#semwebpro <https://twitter.com/hashtag/Semwebpro>`_

Pour plus d'informations, contactez-nous : contact@semweb.pro

