
Logilab rejoint le cluster Digital Place
########################################


:slug: logilab-rejoint-le-cluster-digital-place
:date: 2013/01/07 16:17:16
:tags: BlogEntry

Dans le prolongement de sa politique de développement dans le grand sud-ouest, Logilab a rejoint fin 2012 le `cluster Digital Place`_. Attiré par le dynamisme de ce cluster dédié aux PME du secteur NTIC, Logilab espère ainsi resserrer ses relations avec les acteurs locaux.

.. _`cluster Digital Place`: //www.digitalplace.fr/

