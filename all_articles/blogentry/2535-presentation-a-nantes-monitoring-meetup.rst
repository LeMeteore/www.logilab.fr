
Présentation à Nantes Monitoring Meetup
#######################################


:slug: presentation-a-nantes-monitoring-meetup
:date: 2016/03/14 11:57:33
:tags: BlogEntry

Aujourd'hui, Arthur Lutz présentera ce que nous faisons à Logilab en terme de `supervision active avec les agents Salt et de visualisation de métriques dans Grafana <https://www.logilab.org/blogentry/4858247>`_.

.. image :: https://www.logilab.fr/file/2538?vid=download

La présentation aura lieu ce soir, à partir de 19h à la `Cantine Numérique de Nantes <//cantine.atlantic2.org/>`_, située 11 impasse Juton (Halle de la Madeleine) 44000 Nantes.

`Inscription gratuite, mais obligatoire ! <www.meetup.com/Nantes-Monitoring/events/228892009/>`_

