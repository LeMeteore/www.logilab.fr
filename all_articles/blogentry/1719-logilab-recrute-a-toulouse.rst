
Logilab recrute à Toulouse
##########################


:slug: logilab-recrute-a-toulouse
:date: 2012/07/13 10:56:02
:tags: BlogEntry

Logilab ouvre deux postes d'ingénieurs R&D dans son nouvel établissement implanté à Toulouse, à quelques minutes à pied du métro Ramonville. Pour plus d'information, voir les fiches de postes `CDI - DÉVELOPPEMENT INFORMATIQUE AVANCÉE ET SCIENTIFIQUE`_ et `CDI - DÉVELOPPEMENT INFORMATIQUE AVANCÉE ET WEB SEMANTIQUE`_ .

.. _`CDI - DÉVELOPPEMENT INFORMATIQUE AVANCÉE ET SCIENTIFIQUE`: //www.logilab.fr/card/inge1
.. _`CDI - DÉVELOPPEMENT INFORMATIQUE AVANCÉE ET WEB SEMANTIQUE`: //www.logilab.fr/card/inge2

