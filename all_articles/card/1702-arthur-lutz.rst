
Arthur Lutz
###########


:slug: arthur-lutz
:date: 2012/06/04 14:57:49
:tags: Card

Contacts : 

* courriel : `arthur.lutz@logilab.fr <mailto:arthur.lutz@logilab.fr>`_
* messagerie instantanée jabber/xmpp : `arthur@jabber.logilab.org <xmpp:arthur@jabber.logilab.org>`_ et `salon public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_
* IRC : arthurlutz sur `#salt-fr@irc.freenode.net <//webchat.freenode.net/?channels=pylint>`_
* téléphone :   01 45 32 03 12
* twitter : `@arthurlutz <//twitter.com/arthurlutz>`_
* mastodon : `@arthurlutz@social.logilab.org <https://social.logilab.org/@arthurlutz>`_
* `LinkedIn <//fr.linkedin.com/pub/arthur-lutz/4a/a8a/445>`_
* `github <https://github.com/arthurlogilab>`_
* `bitbucket <https://bitbucket.org/arthurlogilab>`_
* `openhub <https://www.openhub.net/accounts/arthurlutz>`_
* foaf :  `logilab.org <//www.logilab.org/cwuser/alutz?vid=foaf>`_, `cubicweb.org <//www.cubicweb.org/cwuser/alutz?vid=foaf>`_, `semweb.pro <//www.semweb.pro/cwuser/alutz?vid=foaf>`_

