
Adrien Di Mascio
################


:slug: adrien-di-mascio
:date: 2012/11/09 08:41:30
:tags: Card

Contacts : 

* courriel : `adrien.dimascio@logilab.fr <mailto:adrien.dimascio@logilab.fr>`_
* messagerie instantanée jabber/xmpp : `adim@jabber.logilab.org <xmpp:adim@jabber.logilab.org>`_ et `salon public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_
* téléphone :  01 45 32 03 12
* twitter : `@adimasci <//twitter.com/adimasci>`_
* `LinkedIn <//www.linkedin.com/pub/adrien-di-mascio/33/757/3b6>`_
* `github <https://github.com/adimasci>`_
* `bitbucket <https://bitbucket.org/adimascio>`_
* foaf :  `logilab.org <//www.logilab.org/cwuser/adimascio?vid=foaf>`_, `cubicweb.org <//www.cubicweb.org/cwuser/adimascio?vid=foaf>`_

Télécharger le contact en format vcard (à venir)

