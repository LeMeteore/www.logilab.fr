
Stage - Intégration de données externes dans la plateforme CubicWeb
###################################################################


:slug: stage-integration-de-donnees-externes-dans-la-plateforme-cubicweb
:date: 2012/11/12 17:28:48
:tags: Card

Contexte
========

CubicWeb_ est une plateforme logicielle libre permettant de développer
rapidement des applications de gestion de données. Elle s'appuie
fortement sur le modèle métier des informations gérées pour proposer
une approche originale laissant à l'utilisateur final toute latitude
pour naviguer dans le graphe des informations, sélectionner des
données et choisir la vue qui lui permettra de les visualiser. De son
côté, le développeur dispose de fonctionnalités automatisant, à partir
du modèle métier, nombre de tâches habituelles (gestion de
la base relationnelle sous-jacente, génération d'une interface Web par
défaut, migration entre versions successives, etc.)

.. _CubicWeb: //www.cubicweb.org/

Problématique
=============

Les évolutions actuelles de CubicWeb_ visent à faciliter son
intégration avec des données externes, qu'elles proviennent du Web
Sémantique (standards RDF, OWL) ou simplement de fichiers CSV. Dans ce
cadre, il devient primordial de pouvoir récupérer facilement des
données externes, les convertir vers le modèle d'informations interne,
les aligner avec les données internes, les fusionner, etc. Les volumes
de données étant généralement importants, apparaissent également de
classiques besoins de classification automatique, de sélection
semi-dirigée, d'optimisation, etc.

Rôle
====

Intégré à l'équipe de recherche et développement "Web Sémantique" de
Logilab, sous la tutelle d'un ingénieur spécialiste de la gestion de
données, vous l'assisterez dans son travail quotidien et pourrez être
amené à effectuer tout ou partie des travaux suivants, en
collaboration avec l'équipe :

* développer dans la plateforme CubicWeb_ des fonctionnalités
  permettant l'import de données externes,

* travailler à l'alignement des données internes à une application
  CubicWeb_ avec des vocabulaires externes standards (dbpedia_,
  geonames_, freebase_, etc.)

.. _dbpedia: //dbpedia.org/
.. _geonames: //www.geonames.org/
.. _freebase: //www.freebase.com/

Compétences attendues
=====================

* Bonnes connaissances pratiques en programmation objet,
* Connaissance des algorithmes de fouille de données,
* Connaissance des standards du Web Sémantique,
* Anglais technique.
* La connaissance du langage Python_ sera appréciée.

.. _Python: //www.python.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

