
Stage - Ergonomie des interfaces dans les applications CubicWeb
###############################################################


:slug: stage-ergonomie-des-interfaces-dans-les-applications-cubicweb
:date: 2012/11/12 17:25:09
:tags: Card

Contexte
========

CubicWeb_ est une plateforme logicielle libre permettant de développer
rapidement des applications de gestion de données. Elle s'appuie
fortement sur le modèle métier des informations gérées pour proposer
une approche originale laissant à l'utilisateur final toute latitude
pour naviguer dans le graphe des informations, sélectionner des
données et choisir la vue qui lui permettra de les visualiser. De son
côté, le développeur dispose de fonctionnalités automatisant, à partir
du modèle métier, nombre de tâches habituelles (gestion de
la base relationnelle sous-jacente, génération d'une interface Web par
défaut, migration entre versions successives, etc.)

.. _CubicWeb: //www.cubicweb.org/

Problématique
=============

Aujourd'hui, les applications métier construites sur la base de
CubicWeb_ contiennent des volumes importants de données. Se pose alors
la question cruciale de donner à chaque utilisateur l'accès le plus
naturel possible aux informations qui l'intéressent, et ce dans les
différents cas d'utilisation de l'application. Même s'il est
impossible de déterminer une ergonomie standard répondant à tous les
besoins, l'objectif est de dégager des recommandations concrètes qui
seront utilisées lors de la conception des interfaces applicatives.

Rôle
====

Intégré à l'équipe de recherche et développement "Web Sémantique" de
Logilab, sous la tutelle d'un ingénieur spécialiste de l'architecture
Web et de la plateforme CubicWeb_, vous lui apporterez votre soutien
sur les aspects concernant l'ergonomie et serez amené à effectuer tout
ou partie des travaux suivants :

* analyser une ou plusieurs applications CubicWeb_ existantes et
  recueillir les retours de leurs utilisateurs,

* rédiger une série de recommandations concrètes pour améliorer
  l'ergonomie des applications étudiées.

Compétences attendues
=====================

* Bonnes connaissances en ergonomie,
* Bonnes connaissances en conception d'interfaces logicielles,
* Connaissances du Web et de l'architecture de ses applications,
* Anglais technique.

Durée
=====

6 mois

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Lieu
====

Paris ou Toulouse.

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.
Candidatures

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

