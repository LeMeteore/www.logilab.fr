
Logilab et le Web Sémantique
############################


:slug: logilab-et-le-web-semantique
:date: 2011/12/01 15:59:17
:tags: Card

.. image :: //www.w3.org/Icons/SW/sw-horz-w3c.png
  :align: center

Standards du Web Sémantique
===========================

Le W3C a publié depuis une dizaine d'années divers standards (Sparql_,
RDF, OWL_, etc.) dont l'objet est de permettre le partage et la
réutilisation de données publiées sur le Web. Ces standards sont les
briques qui permettent aujourd'hui de construire le Web des données,
ou Web 3.0 : au lieu de publier des informations mises en forme dans
des pages HTML, de plus en plus d'organismes publient directement les
données brutes dans des formats standards, ce qui autorise la
réutilisation de ces données et le développement de nouvelles
applications impossible à concevoir précédemment.

Logilab s'inscrit directement dans cette nouvelle vision du Web et
développe des applications capables de publier des informations aux
formats du Web Sémantique mais surtout d'intégrer des données
provenant de sources externes dans ces mêmes formats.


Plate-forme CubicWeb
====================

Dès 2001, Logilab a travaillé à l'élaboration d'un outil permettant de
réaliser facilement des applications de gestion d'informations à
partir du simple schéma des données manipulées. Au fil des années et
des enrichissements successifs, cet outil est devenu la plate-forme
CubicWeb_.

CubicWeb_ est aujourd'hui une plate-forme proposant :

* une mise à disposition d'objets et de relations respectant le modèle de
  données de l'application,

* une fusion d'informations provenant de sources de données diverses :
  bases de données relationnelles (SQL), gestionnaires de source
  (Mercurial, SVN, etc.), annuaires LDAP, documents RDF, fichiers textuels,

* un mécanisme de requêtes de haut-niveau s'appuyant directement sur
  le modèle de données de l'application et permettant d'exprimer
  n'importe quel type de sélection (de la plus simple à la plus
  complexe),

* deux langages de requêtes : RQL, le langage historiquement développé
  dans CubicWeb_, et Sparql_, le langage standard du Web Sémantique,

* la possibilité de développer des applications pouvant conjointement
  proposer des pages HTML pour une navigation Web classique et des
  documents sémantiques pour une intégration dans le Web Sémantique,

* une séparation nette entre la sélection d'informations (langage de
  requêtes) et la sélection d'une vue permettant d'afficher ces
  informations (HTML, export PDF, export CSV, RDF, graphiques, etc.),

* un mécanisme natif de sécurité permettant, si nécessaire, de définir
  des réglages très fins,

* une gestion industrielle des données et des versions successives de
  l'application (introspection, mécanismes de migration, etc.),

* un développement modulaire des applications par composition à partir
  de cubes existants dans la bilbiothèque de CubicWeb_.

Logilab a publié la plate-forme CubicWeb_ sous une licence libre
en 2007. Depuis, elle poursuit les développements et anime la
communauté qui s'est formée autour de ce logiciel.

Logilab a utilisé la plate-forme CubicWeb_ pour développer diverses
applications, que ce soit pour des intranets d'entreprise ou pour des sites grand public. Le parangon de ces applications est sans conteste le site `//data.bnf.fr/`_ dont les données ont été réutilisées en particulier par le projet OpenCat_.

.. _`//data.bnf.fr/` :  //data.bnf.fr/
.. _CubicWeb : //www.cubicweb.org
.. _Sparql : //www.w3.org/TR/rdf-sparql-query/
.. _RQL : //docs.cubicweb.org/annexes/rql/language
.. _OWL : //www.w3.org/2004/OWL/
.. _OpenCat: /blogentry/1978

