
De l'impact du Logiciel Libre sur l'industrie informatique
##########################################################


:slug: de-l-impact-du-logiciel-libre-sur-l-industrie-informatique
:date: 2011/12/01 15:52:37
:tags: Card

Se libérer de la dépendance envers éditeurs et prestataires
===========================================================    
Le système informatique est aujourd'hui un élément indispensable au fonctionnement de la grande majorité des entreprises, organisations et administrations. Or les éditeurs et prestataires informatiques sont en fait les véritables maîtres de ces infrastructures car ils maintiennent leurs clients dans une dépendance excessive que ces derniers ne peuvent briser qu'au prix d'efforts et de dépenses importantes. Les éditeurs et prestataires tirent parti de cette dépendance. Ils imposent à leurs clients des tarifs élevés et limitent leurs dépenses liées à l'innovation et à la qualité, qui ne sont plus des facteurs déterminants dans la course à l'obtention de nouveaux clients ou à la satisfaction des clients existants.
    
Le Logiciel Libre (en anglais free software) est une nouvelle approche de la    réalisation et de la distribution de logiciels. En redonnant aux utilisateurs le plein contrôle de leur système d'information et leur pleine capacité à choisir parmi l'éventail des prestataires et des éditeurs, le Logiciel Libre promet d'avoir un impact important sur l'industrie informatique en général et la production de logiciel en particulier.
    
Nous étudions ici, en tant qu'un acteur industriel, à la fois prestataire et éditeur, ce qu'est le Logiciel Libre et comment en faire un axe majeur de sa stratégie d'entreprise.
    
Qu'est-ce que le Logiciel Libre ?
=================================      
Libre s'oppose à propriétaire
-----------------------------      
Un logiciel libre s'acquiert comme tout autre bien. La seule différence est que les conditions d'acquisition sont inhabituelles et peuvent prêter à confusion, souvent par méconnaissance du sujet.
      
Les conditions liées à l'acquisition d'un logiciel concernent son obtention, son utilisation, l'accès à son code source, sa modification, sa copie et sa redistribution.
      
En général, les logiciels propriétaires doivent être achetés et sont d'utilisation restreinte (nombre d'utilisateurs simultanés, nombre de poste, durée, etc.). Il est interdit d'accéder à leur code source, interdit de tenter de les modifier et interdit de les copier ou de les redistribuer sous quelque      forme que ce soit.
      
Les Logiciels Libres en revanche, ne s'obtiennent pas nécessairement par un achat et sont d'utilisation illimitée. Il est possible d'accéder à leur code source, de les modifier, de les copier et de les redistribuer.
      
En d'autres termes, rien ne garantit dans le cas des logiciels propriétaires qu'on pourra continuer à en faire l'usage à l'avenir. On pourrait résumer en disant que les logiciels classiques sont loués à leurs propriétaires (lesquels      en restent propriétaires, bien qu'ont ait l'habitude de parler d'achat de logiciel et non de location), alors que les logiciels libres sont acquis une fois pour toute par ceux qui choisissent de les installer et pourront évoluer et être adaptés sans restriction à condition d'en conserver le code source.
      
Une licence d'utilisation particulière
--------------------------------------
Que tout cela soit possible dans le cas des Logiciels Libres n'implique cependant pas que cela puisse être fait dans n'importe quelles conditions. Les conditions exactes d'exercice de ces libertés sont régies par la licence associée au logiciel, comme dans le cas des logiciels propriétaires. Il existe des licences variées pour lesquelles les conditions précises d'utilisation, modification, copie et redistribution peuvent différer.
      
Ainsi, l'accès à un Logiciel Libre peut être payant, mais pour peu que quelqu'un d'autre vous propose le même logiciel gratuitement, ce qui est courant sans être systématique, le "prix de marché" tend rapidement vers zéro. Une distribution telle que Red Hat Linux (compilation de logiciels adaptés pour être facilement utilisables ensembles) est un exemple de produit      payant constitué de Logiciels Libres qui sont disponibles séparément et gratuitement.
      
Toutes les licences de Logiciel Libre autorisent l'accès au code source. Il est cependant important de remarquer que le terme open source ne concerne que      l'accès au code et n'est donc pas équivalent à Logiciel Libre. En particulier, certaines sociétés proposent aujourd'hui d'accéder à tout ou partie du code source de leur produit, mais sans accorder aucune des autres libertés mentionnées ci-dessus et parfois même en imposant des contraintes de    confidentialité.      
      
La grande majorité des licences de Logiciel Libre autorisent la modification du code, la différence se faisant ensuite dans les conditions associées à la redistribution.
      
La licence GPL
--------------
Dans le cas de la licence GPL, très répandue, la redistribution doit se faire dans les mêmes termes que l'acquisition y compris pour les travaux dérivés. Quelqu'un qui télécharge gratuitement un Logiciel Libre sous GPL peut     parfaitement le revendre, mais ne peut pas lui appliquer une autre licence et l'acheteur pourra donc bénéficier des mêmes avantages que son fournisseur. En particulier, les modifications apportées à un Logiciel Libre sous GPL doivent obligatoirement être sous GPL si et seulement si elles sont redistribuées.
      
GPL est donc une licence, acceptée par le "client" au moment de l'acquisition, dont le but est de garantir que personne ne puisse s'approprier et "enfermer" le logiciel.
      
Il est donc aisé de comprendre que les Logiciels Libres et la licence GPL ont de multiples avantages. Ils permettent aux utilisateurs de ne plus dépendre d'un éditeur pour faire évoluer un logiciel, le corriger ou assurer une compatibilité ascendante et la pérennité des données. La compétition entre éditeurs et entre prestataires est replacée sur le terrain de la compétence      et de la qualité de service puisqu'aucune restriction intrinsèque au logiciel utilisé ne rend l'utilisateur dépendant d'un fournisseur particulier.      
      
Dans le cas de Logiciels Libres gratuits, la part du budget consacrée à l'achat de licence peut être reportée, pour partie au moins, sur l'adaptation et la formation, qui permettent au final de disposer d'une solution mieux intégrée au reste du système d'information et mieux maîtrisée par les      utilisateurs. Ces avantages ne sont pas à négliger quant on voit la proportion de projets informatiques qui n'atteignent pas leur objectifs faute d'avoir suffisamment accordé d'attention à ces deux aspects.
    
Quel modèle économique stable pour le Logiciel Libre ?
======================================================
Partager et réutiliser pour assurer la qualité au moindre coût
--------------------------------------------------------------
La première question que posent ceux qui découvrent le principe du Logiciel Libre concerne sa pérennité. En effet, la gratuité de nombreux Logiciels Libres peut paraître aller à l'encontre de la capacité de leurs éditeurs à développer de nouveaux logiciels.
      
Une première remarque importante est que pour un usage donné la plupart des logiciels propriétaires en concurrence offrent des fonctionnalités presque identiques et ne diffèrent véritablement que par leurs lacunes, leurs défauts et leurs trous de sécurité. La cause de cet état de fait est le coût très      élévé de la production de logiciel associé à un prix de marché faussé à la baisse par l'acceptation quasi-générale d'une qualité extrêmement faible.
      
Le Logiciel Libre, en favorisant la réutilisation et le partage des composants et logiciels d'infrastructure, permet au contraire la réduction des investissements de développement, l'accroissement de la qualité et la richesse fonctionnelle. Les Logiciels Libres qui répondent à un véritable besoin réunissent souvent des dizaines, voire des centaines de développeurs,  concepteurs, testeurs et intégrateurs, pour des périodes qui s'étendent parfois sur plusieurs années. Peu d'éditeurs peuvent se permettre de maintenir des équipes d'une telle taille pour développer, tester et adapter leurs logiciels.
    
Les SSLL moteurs du Logiciel Libre
----------------------------------
Une fois acquis le bien-fondé des bienfaits du modèle de développement du Logiciel Libre, il convient de s'interroger sur la structuration à grande échelle de cette nouvelle approche. En effet, pour qu'elle soit viable il est évident que même s'ils ne sont pas directement rétribués pour la vente du logiciel, ceux qui participent à ces projets doivent en tirer profit. Les      utilisateurs qui bénéficient des avantages du Logiciel Libre et ont intérêt à le voir se développer recherchent donc rapidement leur place dans l'écosystème.
      
De nombreux rôles sont endossables si l'on souhaite apporter une contribution technique : du développement à la rédaction de documentation en passant par les tests et la maintenance de site web et d'outils facilitant la collaboration et la coordination.
      
Pour un utilisateur classique, n'ayant pas vocation à développer ou maintenir des compétences de ce type au sein de son organisation, la situation est différente. Une solution est alors de considérer les sociétés de services qui participent au logiciel libre comme des bureaux de change ou des intermédiaires entre les utilisateurs et les communautés de développement. L'argent investi dans le développement via ces SSLL est souvent converti pour partie en temps consacré à des projets de Logiciel Libre. Il est à souligner la différence importante entre une société qui utilise du logiciel libre pour      répondre aux besoins de ses clients mais ne contribue pas en retour au monde du Logiciel Libre et celles qui en sont de véritables acteurs. Ces dernières sont conscientes du fait que sans contribution, les sources de Logiciel Libre ne peuvent pas perdurer éternellement. Les utilisateurs qui souhaitent      bénéficier des avantages du Logiciel Libre, doivent donc choisir entre tirer un profit à court terme du phénomène et contribuer à asseoir un modèle qui leur rend leur prérogatives en faisant perdurer des sociétés véritablement engagées.
      
Le plus probable est bien évidemment qu'un certain noyau dur continue d'exister alors que d'autres sociétés plus grosses jouent le rôle de prédateurs et utilisent les logiciels produits sans participer elles-mêmes à des projets du même type. Cette organisation contribuera à ralentir les développements et à freiner l'abord des problèmes beaucoup plus complexes qui      demandent des équipes importantes pour des durées longues.
    
La formation de communautés d'intérêt
=====================================
Un client et des besoins
------------------------
Un frein potentiel au développement à façon de Logiciels Libres est la crainte pour le commanditaire de voir ses concurrents s'approprier le résultat final à moindre coût. De même, certaines sociétés peuvent hésiter à se lancer dans le      développement de logiciels si elles savent que leurs clients n'accepteront de les acquérir que sous une licence libre. La formation de communautés d'intérêt telle que décrite ci-après est une organisation des relations client-prestataire qui permet à tous de profiter des avantages du logiciel libre sans laisser l'occasion à ces craintes de devenir justifiées.
      
Un client qui souhaite réaliser un logiciel contacte son fournisseur pour lui demander de développer les fonctionnalités A,B,C dans les limites d'un budget fixe. Le fournisseur lui répond qu'il ne peut réaliser que A et B dans les limites de ce budget et en atteignant les standards de qualité du logiciel      libre. Au terme du développement, le client obtient le résultat sous une licence de logiciel libre.
      
On pourrait en arriver au même point si un fournisseur décide d'investir dans le développement d'un logiciel qu'il propose ensuite à ses clients sous une licence de logiciel libre. Plutôt que de commander le développement, le client      intéressé achète le logiciel déjà réalisé et la situation est similaire à celle du cas précédent. 
      
A ce moment, le client peut s'il le souhaite s'adresser à un autre fournisseur ou poursuivre le développement en interne. Il peut aussi mettre le logiciel à disposition du public, mais s'il en a payé le développement, il est probable      qu'il ne souhaite pas le faire. On remarque au passage qu'un établissement public pourrait faire le choix opposé.
      
De son côté, le fournisseur peut s'il le souhaite publier le logiciel, mais il ne souhaitera pas mécontenter son client ni faciliter le travail de ses concurrents.
      
Le plus probable est donc que le logiciel reste confidentiel, bien qu'il soit libre et ceci sans accord ou convention particulière entre les parties, mais du simple fait de leur propre intérêt.
      
Un deuxième client et des besoins similaires
--------------------------------------------
Par la suite, admettons qu'un nouveau client demande au même fournisseur un logiciel ayant les fonctionnalités A,B,C,D. Ce dernier pourra le mettre en relation avec son premier client puisqu'ils semblent avoir des besoins      communs. Après entente, le fournisseur réutilisera ce qui a déjà été développé et y ajoutera les fonctionnalités C et D avant de faire parvenir le résultat aux deux clients.
      
Le premier client aura donc obtenu toutes les fonctionnalités de A à D pour le seul coût de A et B. Le deuxième client aura lui aussi obtenu toutes les fonctionnalités de A à D pour le seul coût de C et D. Le fournisseur quant à lui aura été payé convenablement pour faire un travail de qualité.
      
Le principe est donc de former des communautés de clients ayant des intérêts communs de manière à ce qu'ils puissent partager la charge d'un développement logiciel.
      
Une communauté d'intérêt en évolution     
-------------------------------------
L'évolution naturelle de ce genre de communautés est ensuite d'introduire de nouveaux fournisseurs capables d'apporter leur expertise pour résoudre des problèmes que les fournisseurs déjà membres ne savent pas traiter.
      
Après un certain temps d'évolution, les composants de bases de la solution logicielle développée ont été suffisamment réutilisés et n'offrent plus d'avantage concurentiel particulier aux clients membres. Il devient alors intéressant de les partager avec un plus grand nombre et de les publier hors de la communauté.
      
De même si la taille de la communauté dépasse un certain seuil, les intérêts viennent à diverger et des sous-communautés se forment. Cette scission indique qu'il est probablement temps de mettre à disposition du public le logiciel d'origine autour duquel la communauté s'était formée. D'autant que si la      communauté grandit, comme tous les membres acquièrent le logiciel sous une licence libre, tous ont le droit de le redistribuer et les chances pour qu'un concurrent ou un "extérieur" se procure le logiciel croissent elles aussi. Plutôt que de voir un projet et des développements reprendre sur la      base d'une version "échappée de l'enclos" il est préférable d'éviter une scission de la base de code et de constituer une communauté ouverte au public autour de ce logiciel.
    
Lexique
=======      
Open Source : 
    licence autorisant la consultation du code source, sans nécessairement imposer les droits de copie et de redistribution. Le Logiciel Libre garantit donc plus de droits aux utilisateurs.
      
Freeware : 
	logiciel gratuit. Ne garantit pas les droits d'accès au
	code source, de copie et de redistribution. Ne pas confondre avec Logiciel Libre.
      
Free Software : 
	Dans Free Software,
	free doit s'entendre au sens de
	libre et non de gratuit. Le terme portant à confusion, on a	cherché à utiliser Open Source,
	qui ne désigne malheureusement qu'une fraction des libertés
	garanties par le Logiciel Libre.
      
Shared Source : 
	Terme issu de la tentative d'un grand éditeur de contrer le développement du Logiciel Libre en accordant un accès au code source de ses logiciels moyennant la signature d'un accord de confidentialité. On peut imaginer que les participants feront par la suite l'objet de poursuites au nom  des brevets logiciels s'ils contribuent à des projets de Logiciel Libre.
      
Logiciel Libre ou Propriétaire : 
      Libre ne s'oppose pas à commercial, mais à propriétaire. Un logiciel    libre peut être vendu comme peut l'être un logiciel propriétaire. Seules les conditions associées sont différentes.
      
SSLL : 
      Société de Service spécialisée en Logiciel Libre. 

    
En savoir plus...
=================
`APRIL <//www.april.org/>`_: Association pour la Promotion et la Recherche en Informatique Libre.
      
`FSF Europe <//www.fsfeurope.org/>`_: Free Software Foundation Europe, pour en savoir plus sur le `Logiciel Libre <//www.fsfeurope.org/documents/freesoftware.fr.html>`_.
      
`AFUL <//www.aful.org/>`_: Association Francophone des Utilisateurs de Logiciels Libres, pour en savoir plus sur les `brevets logiciels <//www.aful.org/ressources/articles/brevetabilite-logiciels>`_.
      
**Modèles économiques** : Les entreprises membres de l'APRIL ont publié un `livre blanc des modèles économiques <//www.april.org/articles/livres-blancs/modeles-economiques-logiciel-libre/>`_ du logiciel libre.
      
**Licences** : 	On pourra se référer à un `article de l'ADAE <//www.adae.gouv.fr/article.php3?id_article=280>`_ et un `rapport de l'ATICA (maintenant ADAE) <//www.atica.pm.gouv.fr/pages/documents/fiche.php?id=1450&id_chapitre=8&id_theme=55&letype=0>`_ pour un comparatif des licences de Logiciels Libres.

      
**Dossiers de l'ADAE** : `Dossiers de l'Agence pour le Développement de l'Administration Electronique <//www.atica.pm.gouv.fr/pages/documents/liste_liaison_theme.php?id_chapitre=7&id_theme=21>`_
      
`FFII <//www.ffii.org/index.fr.html>`_ : Association pour une Infrastructure Informatique Libre qui a pour objet de promouvoir les savoirs dans le domaine du traitement des données et lutte contre une mise en oeuvre aveugle des brevets en europe, en particulier dans le domaine du logiciel.

