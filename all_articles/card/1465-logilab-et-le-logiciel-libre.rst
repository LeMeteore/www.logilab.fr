
Logilab et le Logiciel Libre
############################


:slug: logilab-et-le-logiciel-libre
:date: 2011/12/01 15:52:37
:tags: Card

Le système informatique est aujourd'hui un élément indispensable au fonctionnement de la grande majorité des entreprises, organisations et administrations. Or les éditeurs et prestataires informatiques sont en fait les véritables maîtres de ces infrastructures car ils maintiennent leurs clients dans une dépendance excessive que ces derniers ne peuvent briser qu'au prix d'efforts et de dépenses importantes. Les éditeurs et prestataires    tirent parti de cette dépendance. D'une part en imposant à leurs clients des tarifs élevés et d'autre part en limitant leurs dépenses liées à l'innovation et à la qualité, qui ne sont plus des facteurs déterminants dans la course à l'obtention de nouveaux  clients ou à la satisfaction des clients existants. 

Le Logiciel Libre (en anglais *free software*) est une nouvelle approche de la réalisation et de la distribution de logiciels. En redonnant aux utilisateurs le plein contrôle de leur système d'information et leur pleine capacité à choisir parmi l'éventail des prestataires et des éditeurs, le Logiciel Libre promet d'avoir un impact important sur l'industrie informatique en général et la production de logiciel en particulier.

**Logilab** est un acteur du Logiciel Libre et met à disposition certains de ses développements sur ses sites `logilab.org <//www.logilab.org/>`_ et `cubicweb.org <//www.cubicweb.org/>`_. Dans la mesure du possible, **Logilab** développe, utilise, adapte et contribue aux Logiciels Libres.

**Logilab** fournit pour tous les Logiciels Libres qu'elle développe des paquets permettant de les installer aisément sur une distribution `Debian GNU/Linux <//www.debian.org/>`_. Cette distribution est utilisée sur tout le parc informatique de **Logilab**. Elle a la particulatité d'être non commerciale et constituée exclusivement de Logiciels Libres, contrairement à d'autres distributions du système d'exploitation Linux.

Pour en savoir plus sur le Logiciel Libre, reportez-vous à notre article intitulé `De l'impact du Logiciel Libre sur l'industrie informatique </impact-logiciel-libre>`_. Pour en savoir plus sur Debian, reportez-vous à notre article intitulé `Pourquoi choisir Debian pour déployer Linux dans son entreprise ? </pourquoi-debian>`_.

Offre libre
-----------
L'`AFUL <https://www.aful.org/>`_, qui a pour principal objectif de promouvoir les logiciels libres ainsi que l'utilisation des standards ouverts, a attribué à **Logilab** la note "A" - la plus élevée - pour l'offre CubicWeb. 

Pour en savoir plus, rendez-vous sur le site dédié : //offrelibre.com/

