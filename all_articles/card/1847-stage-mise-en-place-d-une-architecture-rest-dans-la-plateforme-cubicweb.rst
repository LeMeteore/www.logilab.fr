
Stage - Mise en place d'une architecture REST dans la plateforme CubicWeb
#########################################################################


:slug: stage-mise-en-place-d-une-architecture-rest-dans-la-plateforme-cubicweb
:date: 2012/11/12 17:10:52
:tags: Card

Contexte
========

CubicWeb_ est une plateforme logicielle libre permettant de développer
rapidement des applications de gestion de données. Elle s'appuie
fortement sur le modèle métier des informations gérées pour proposer
une approche originale laissant à l'utilisateur final toute latitude
pour naviguer dans le graphe des informations, sélectionner des
données et choisir la vue qui lui permettra de les visualiser. De son
côté, le développeur dispose de fonctionnalités automatisant, à partir
du modèle métier, nombre de tâches habituelles (gestion de
la base relationnelle sous-jacente, génération d'une interface Web par
défaut, migration entre versions successives, etc.)

.. _CubicWeb: //www.cubicweb.org/

Problématique
=============

La dynamique actuelle autour des applications logicielles, en
particulier la multiplication des services en ligne (*Sofware as a
Service*) et du paiement à l'utilisation (*pay per use*), ainsi que le
recours de plus en plus fréquent à l'informatique en nuage (*cloud
computing*), rend plus que jamais nécessaire de concevoir des
applications respectant les principes fondateurs du Web,
principalement l'architecture REST (*Representational State
Transfer*). Dans ce contexte, l'amélioration de la plateforme
CubicWeb_, et donc des applications qu'elle héberge, passe par une mise
en œuvre aussi rigoureuse que possible des contraintes de
l'architecture REST.

Rôle
====

Intégré à l'équipe de recherche et développement "Web Sémantique" de
Logilab, sous la tutelle d'un ingénieur spécialiste de la
programmation Web et de la plateforme CubicWeb_, vous l'assisterez dans
son travail quotidien et serez amené à effectuer tout ou partie
des travaux suivants, en collaboration avec l'équipe :

* dresser l'état des lieux du respect des principes REST au
  sein de plateforme CubicWeb_,

* refondre dans CubicWeb_ le code nécessaire pour respecter ces
  principes,

* vérifier le fonctionnement d'applications existantes après cette
  refonte.

Compétences attendues
=====================

* Bonnes connaissances de la programmation Web et du protocole HTTP,
* Bonnes connaissances pratiques en programmation objet,
* Anglais technique,
* La connaissance du langage Python_ ou de l'architecture REST sera appréciée.

.. _Python: //www.python.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

