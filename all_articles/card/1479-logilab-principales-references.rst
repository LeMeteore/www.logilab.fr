
Logilab - principales références
################################


:slug: logilab-principales-references
:date: 2011/12/01 15:52:37
:tags: Card

Informatique Scientifique
_________________________

EDF R&D, CEA, France Télécom R&D, EADS, Eurocopter, Snecma, NTT, ESRF, Arcelor, etc.

Gestion de connaissances
________________________

CEA, Ministère de l'Intérieur, Préfecture de Picardie, Préfecture de Corrèze, Préfecture d'Indre-et-Loire, Préfecture de Champagne-Ardenne, Préfecture de la Marne, Ministère de l'Agriculture, École Centrale Paris, etc.

Formation
__________

Alcatel Space, Andra, Arcelor, CNRS, ESRF, EDF R&D, CEA, France Télécom, EADS Airbus, Michelin, Schlumberger, Bouygues, Ministère de l'Intérieur, Conseil Général de la Gironde, Archives départementales des Yvelines, Thalès, Safran, Parkeon, Nagravision, GL Trade, CERFACS, etc.

Méthodes agiles
_______________

Ministère de l'Éducation Nationale, Reuters, Ministère de la   Défense, Bruker, Bouygues Télécom, ESRF, etc.

Logiciel libre
_______________

www.logilab.org, Debian, PyPy, pylint, Narval®, Zope/Plone/CPS, etc.

