
Société
#######


:slug: societe
:date: 2011/12/01 15:52:37
:tags: Card

Informatique scientifique, web sémantique et agilité
-----------------------------------------------------

Logilab **développe** des **logiciels**, et propose du **conseil** et des **formations** de haut niveau dans les domaines de l'`informatique scientifique </informatique-scientifique>`_  et du `web sémantique </web-semantique>`_, en utilisant des `outils et méthodes agiles </outils-methodes-agiles>`_.


.. raw:: html

   <div>
   <a href="/file/2182/raw/logilab-3volets.min.pdf"><img src="/file/2183/raw" width="400px"></a>
   </div>

Compétences
-----------
	
Logilab s'est `spécialisée </competences>`_ dans l'utilisation de certains outils (`Python </python>`_, `CubicWeb </gestion-connaissances>`_, `Debian </debian>`_, etc.) et techniques (génie logiciel, programmation multi-paradigmes, statistiques, logique, etc.) pour les appliquer aux domaines de l'`informatique scientifique </informatique-scientifique>`_ (simulation numérique, calcul hautes performances, analyse de données, etc.) et du `web sémantique </web-semantique>`_ (`gestion de connaissances </gestion-connaissances>`_, agrégation de bases de données, outils de recherche et de veille, etc.).

.. . image:: //www.logilab.fr/file/1412?vid=download
  :align: left

.. . image:: //www.logilab.fr/file/2225/raw/cubicweb-logo.png
  :align: left

Logilab Campus
--------------

Les `formations </formations>`_ de **Logilab Campus** s'adressent en priorité aux ingénieurs, chercheurs et techniciens désireux d'accroître leur culture informatique. Elles couvrent des sujets variés (Python,  conception orientée objet, méthodes de développement agiles, etc.) et sont systématiquement adaptées aux besoins des participants, qu'il  s'agisse de sessions intra ou inter-entreprises.

Logiciel Libre
----------------

Logilab est un acteur du `Logiciel Libre </logiciel-libre>`_ et met à disposition certains de ses développements sur ses sites `logilab.org <//www.logilab.org/>`_ et `cubicweb.org <//www.cubicweb.org/>`_.

Innovation
------------

Logilab est une entreprise innovante, membre des pôles de compétitivité `Systematic Paris Région <//www.systematic-paris-region.org/>`_ et `Aerospace Valley <//www.aerospace-valley.com/>`_.

.. , au sein desquels elle contribue aux groupes thématiques `Outils de Conception et Développement de Systèmes <//www.systematic-paris-region.org/fr/ocds>`_ et `Logiciels Libres <//www.systematic-paris-region.org/fr/logiciel-libre>`_.

Logilab participe à plusieurs projets collaboratifs co-financés par l'ANR, le FUI et les programmes européens. Elle s'est vu attribuer par `OSEO Innovation <//www.oseo.fr/>`_ le label Technologie Clef.

.. . image:: //www.logilab.fr/file/1410/raw/systematic-logo.png
  :align: center
  :width: 400px

