
Stage - Amélioration ergonomique de  Simulagora, une plateforme de simulation numérique dans les nuages
#######################################################################################################


:slug: stage-amelioration-ergonomique-de-simulagora-une-plateforme-de-simulation-numerique-dans-les-nuages
:date: 2012/11/12 17:35:10
:tags: Card

Contexte
========

L'arrivée des nuages de serveurs (*cloud computing*) a remis fortement
en cause la façon d'aborder l'informatique et plus particulièrement le
calcul scientifique. En effet, aujourd'hui, de grandes structures,
comme Amazon ou Google aux États-Unis, mettent à disposition sur
Internet un réseau de serveurs pouvant proposer une puissance de
calcul phénoménale. Ces serveurs sont loués à l'utilisation à un prix
dérisoire, ce qui permet à n'importe qui d'avoir accès à une puissance
de calcul quasiment illimitée. Dans le domaine scientifique, cela
signifie que des simulations qui, hier, semblaient inaccessibles sont
désormais possibles. Toutefois, le processus permettant d'effectuer
des simulations dans un nuage de serveurs est loin d'être simple, et
ses nombreuses opérations peuvent rapidement dérouter les néophytes
peu versés dans l'informatique système et l'administration de
serveurs.

Problématique
=============

Partant de ce constat, Logilab développe une plateforme de simulation
numérique, `Simulagora <https://www.simulagora.com/>`_, mettant à la disposition de scientifiques un pilote et un
gestionnaire de cas de calcul. Les divers solveurs et outils
nécessaires ayant été préinstallés et configurés, l'utilisateur n'a
plus qu'à téléverser ses données, lancer ses calculs puis télécharger
les résultats.

L'objectif des travaux proposés ici est d'améliorer l'ergonomie de la
plateforme de simulation afin de faciliter sa prise en main et son
utilisation, ainsi que d'étoffer l'offre de solveurs et outils
proposés sur les serveurs de calcul. La plateforme de simulation
numérique est développée en Python_ en s'appuyant sur la plateforme
CubicWeb_.

.. _Python: //www.python.org/
.. _CubicWeb: //www.cubicweb.org/

Rôle
====


Intégré à l'équipe de recherche et développement "Simulation
Numérique" de Logilab, sous la tutelle d'un ingénieur spécialiste de
la mise en œuvre de codes de calcul et de la plateforme CubicWeb, vous
l'assisterez dans son travail quotidien et pourrez être amené à
effectuer tout ou partie des travaux suivants, en collaboration avec
l'équipe :

* contribuer au développement de la plateforme de simulation numérique `Simulagora <https://www.simulagora.com/>`_
  dans le cloud,

* mettre en œuvre sur la plateforme une simulation numérique complète
  illustrant ses qualités.

Compétences attendues
=====================

* Bonnes connaissances pratiques en programmation objet,
* Connaissance de l'architecture et des techniques du Web (HTML,
  Javascript, etc.)
* Connaissances générales en simulation numérique,
* Connaissances de base de l'administration système sous Linux,
* Anglais technique,
* Les connaissances du langage Python_ et de la virtualisation de serveur
  seront appréciées.

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

