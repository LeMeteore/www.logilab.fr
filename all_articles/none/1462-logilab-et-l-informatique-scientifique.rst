
Logilab et l'informatique scientifique
######################################


:slug: logilab-et-l-informatique-scientifique
:date: 2011/12/01 15:52:37
:tags: Card
:category: none

**Logilab** a développé diverses applications scientifiques pour ses clients : chaînes de calcul pour l'aérodynamique, détection de défauts par traitement d'image, simulations pour la prévision des marchés boursiers, etc.

==========================================
Python pour les applications scientifiques
==========================================

Python est un langage qui convient idéalement aux applications    scientifiques puisqu'il permet de réutiliser des codes de calcul existants, écrits en Fortran ou C++, en les intégrant dans une application multi-plates-formes moderne, disposant d'une interface graphique ergonomique et des capacités d'échange de données indispensables à une utilisation dans un environnement ouvert.

====================
Salomé et Code Aster
====================

**Logilab** a par ailleurs participé au cours des dernières années aux projets `Salomé <://www.salome-platform.org/>`_ et `Code_Aster <://www.code-aster.org/>`_, d'abord en formant à Python une part des équipes de développements, puis en intervenant sur des aspects liés à la qualité et en intégrant des logiciels tiers. **Logilab** a aujourd'hui l'expérience nécessaire pour aider des bureaux d'études à prendre en main ces outils et pour développer des applications métiers spécifiques.

