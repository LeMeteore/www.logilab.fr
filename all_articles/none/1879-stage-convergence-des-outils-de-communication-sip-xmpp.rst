
Stage  - Convergence des outils de communication SIP/XMPP
#########################################################


:slug: stage-convergence-des-outils-de-communication-sip-xmpp
:date: 2013/01/07 12:34:45
:tags: Card
:category: none

Contexte
========

Il existe aujourd'hui de nombreux moyens de communications à
disposition des personnes travaillant dans une entreprise comme
Logilab : courriel, messagerie instantanée et téléphone par
Internet. Ces différents moyens de communication sont peu
interopérables en eux-mêmes ; cependant, des outils propriétaires
existent pour mettre en œuvre des plates-formes de communication
convergentes (Skype, Microsoft Lync, etc.)

Du côté du logiciel libre, et sur les systèmes Debian_ en particulier,
pour chaque canal de communication, il existe de nombreux outils
disponibles, mais il existe assez peu de solutions permettant de les
faire interagir pour proposer une infrastructure de communication
unifiée et cohérente.

Au sein de Logilab, nous avons déployé ces canaux de manière
indépendante : nous disposons d'un serveur Freeswitch_ pour
l'infrastructure SIP, d'un serveur ejabberd_ pour la communication
Jabber_, et d'une infrastructure Postfix_ pour les courriels.

.. _Debian: ://www.debian/org/
.. _Freeswitch: ://www.freeswitch.org/
.. _ejabberd: ://www.ejabberd.im/
.. _Postfix: ://www.postfix.org/
.. _Jabber: ://www.jabber.org/

Problématique
=============

L'objectif de ce stage est de mettre au point sur un environnement de
type *cloud* OpenStack_ une infrastructure combinée SIP + Jabber, en
s'inspirant de la `présentation de Daniel Pocock`_ donnée lors d'une
`Mini-DebConf`_ à Paris en 2012.

.. _`présentation de Daniel Pocock`: ://fr2012.mini.debconf.org/slides/debian-voip.pdf
.. _`Mini-DebConf`: ://fr2012.mini.debconf.org/
.. _OpenStack: ://www.openstack.org/

Rôle
====

Intégré à l'équipe de recherche et développement "Outils et systèmes"
de Logilab, vous assisterez un ingénieur expérimenté dans son travail
quotidien et pourrez être amené à effectuer tout ou partie des travaux
suivants, en collaboration avec l'équipe :

- s'approprier les outils de déploiement d'infrastructure système dans
  un *cloud* OpenStack_,

- étudier les différentes solutions de serveur et proxy SIP, les PBX
  qui peuvent y être rattachés, et les serveurs Jabber_, dans le cadre
  d'un déploiement dans des systèmes Debian_,

- éventuellement participer au *packaging* Debian_ d'outils actuellement
  non disponibles sous forme de paquet officiel,

- mettre en place une infrastructure de test unifiant les protocoles
  Jabber et SIP,

- développer une configuration SaltStack_ pour déployer
  l'infrastructure.

.. _SaltStack: ://www.saltstack.org/

Compétences attendues
=====================

- Bonnes connaissances pratiques en programmation objet.
- Bonne connaissance de l'administration système sous Linux avec
  Debian_.
- Anglais technique.
- Les connaissances du langage Python_ et de la virtualisation de
  serveur seront appréciées.
- Idéalement, connaissances de SIP, Freeswitch_ et Jabber_.
- Idéalement, connaissances du système de paquet et de la communauté
  Debian_.
- Idéalement, connaissances SaltStack_, OpenStack_ et Nagios_.

.. _Python: ://www.python.org/
.. _Nagios: ://nagios.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

