
Compétences
###########


:slug: competences
:date: 2011/12/01 15:52:37
:tags: Card
:category: none

Outils
======

Python
-------
`Python <://www.python.org>`_ est une plate-forme de développement moderne qui constitue notre outil principal pour la réalisation de logiciels. **Logilab** contribue à la communauté Python, co-organise la conférence `EuroPython <://www.europython.org>`_, a co-fondé le Python Business Forum et co-organise `EuroSciPy <://www.euroscipy.org>`_ qui a réuni à l'ENS près de 180 personnes en 2011.

Debian GNU Linux
----------------
Les solutions développées par **Logilab** s'intègrent toutes avec la distribution `Debian GNU/Linux <://www.debian.org>`_, qui est une distribution de Linux non commerciale, constituée uniquement de logiciels libres, facile à administrer et à déployer sur de grands parcs informatiques.


Mercurial
---------
`Mercurial <://mercurial.selenic.com>`_ est un gestionnaire de sources moderne et efficace, utilisé par un nombre croissant de projets importants, parmi lesquels Python, Mozilla Firefox, OpenSolaris, etc. Mercurial fonctionne en mode distribué, ce qui présente de nombreux avantages comparés aux outils tels que CVS, Subversion, Perforce, etc. Écrit en `Python <://www.python.org>`_, Mercurial est développé par une communauté dynamique à laquelle **Logilab** participe activement. Logilab développe également des outils autour de Mercurial tels que `hgview <://www.logilab.org/project/hgview>`_.

Salt
-------
`Salt <://saltstack.com/community/>`_ est un logiciel libre de gestion de configuration et d'automatisation d'infrastructure qui fonctionne sur la plupart des systèmes (Unix, Windows, MacOSX, embarqué) et est utilisé par des acteurs majeurs du cloud et des réseaux sociaux tels que Rackspace et LinkedIn. **Logilab** participe
au développement de Salt, est le partenaire français de SaltStack et promeut
l'approche *Test-Driven Infrastructure*.


Logiciel Libre
---------------
Dans la mesure du possible, **Logilab** développe, utilise, adapte et contribue aux `Logiciels Libres </logiciel-libre>`_. Logilab et ses experts participent au large mouvement du Logiciel Libre depuis la généralisation d'Internet au milieu des années 90.

.. XML
.. ---
.. `XML <://www.w3.org/xml>`_ est une famille de langages adaptés à l'échange de données et la structuration de documents au sens large. **Logilab** a participé au développement de la bibliothèque XML de Python.


Offre libre
-----------
L'`AFUL <https://www.aful.org/>`_, qui a pour principal objectif de promouvoir les logiciels libres ainsi que l'utilisation des standards ouverts, a attribué à **Logilab** la note "A" - la plus élevée - pour l'offre CubicWeb. 

Pour en savoir plus, rendez-vous sur le site dédié : ://offrelibre.com/

Techniques 
==========
Génie Logiciel
--------------
Le génie logiciel est le métier principal des experts de **Logilab**. Ils en ont une connaissance étendue et savent faire les bons choix lors des phases de conception, en amont de la programmation, en s'appuyant sur des aspects théoriques et sur l'expérience acquise au cours de plusieurs années consacrées à la production logicielle. `Unified Modeling 
Language <://www.uml.org/>`_ et `méthodes agiles <://www.agilealliance.com>`_ comptent parmi les techniques utilisées.

Intelligence artificielle - Mathématiques
-----------------------------------------
Les experts de **Logilab** ont tous une formation d'ingénieur qui leur permet d'aborder les problèmes concrets que posent les différents domaines d'application, avec le recul qu'apportent de solides bases théoriques. Logique, réseaux de neurones, statistiques, agents intelligents, reconnaissance des formes, apprentissage, traitement d'images, théorie des langages, réseaux sémantiques, traitement du signal, etc. sont autant d'outils à leur disposition pour construire la solution la plus adaptée à un problème.

.. Programmation multi-paradigmes et couplage
.. ------------------------------------------
.. La faveur de Logilab va aux langages de programmation les plus expressifs et les plus efficaces, en particulier les langages multi-paradigmes faciles à étendre via C/C++ et à coupler avec d'autres langages. **Logilab** a donc lancé le projet `Python-Logic </python-logic>`_ pour ajouter des capacités de programmation logique à Python, et encourage la programmation par aspects et contrats, ainsi que les évolutions actuelles de Python que sont `Stackless <://www.stackless.com>`_ pour la programmation concurrente et `Python-in-Python <://www.codespeak.net/pypy>`_ pour un interpréteur minimal et facilement adaptable. **Logilab** participe au projet `PyPy <://www.pypy.org>`_ qui préfigure les évolutions futures de Python.Qualité. **Logilab** respecte au quotidien une démarche qualité rigoureuse, qui assure des développements et des services efficaces et fiables sur lequels faire reposer sans crainte son activité.

Domaines 
========
Informatique avancée et scientifique
------------------------------------
L'informatique avancée et scientifique concerne la mise en œuvre de techniques à fort caractère mathématique ou issues de l'intelligence artificielle. En particulier, l'utilisation d'agents intelligents, le traitement du langage naturel, la simulation, l'analyse de données, le `calcul scientifique </informatique-scientifique>`_, etc.

Gestion des connaissances
-------------------------
La gestion des connaissances recouvre ce qui concerne la création, l'acquisition, la manipulation, le traitement, la transformation et la visualisation des connaissances. En particulier, on peut citer la veille technologique, l'automatisation des processus de travail, la capitalisation et l'interrogation des connaissances, etc. La plupart des problèmes de rédaction et de travail collaboratifs peuvent être abordés et résolus dans le cadre plus général de la gestion des connaissances. `En savoir plus </gestion-connaissances>`_.

Web sémantique
---------------

Le `web sémantique </web-semantique>`_, mieux nommé web des données, prolonge le
web des documents en identifiant les données unitaires par des URL et en les échangeant via le protocole HTTP en respectant des vocabulaires standardisés. Il en résulte une forme de base de données mondiale, au sein de laquelle l'effet de réseau joue à plein, de telle sorte que la valeur des données dépend des liens qui les associent aux autres données disponibles.
Depuis sa création, Logilab a oeuvré pour l'avènement du Web sémantique avec le développement d'outils (`CubicWeb <://www.cubicweb.org>`_), la réalisation de projets phares (`data.bnf.fr <://data.bnf.fr>`_) et la diffusion des connaissances (conférence `semweb.pro <://www.semweb.pro>`_). 

Tous les ans, **Logilab** organise `semweb.pro <://www.semweb.pro>`_, rendez-vous devenu incontournable des membres de la communauté, des sociétés innovantes et des industriels désireux de mettre en œuvre les nouvelles techniques du Web Sémantique.

