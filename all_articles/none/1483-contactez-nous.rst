
Contactez nous
##############


:slug: contactez-nous
:date: 2011/12/01 15:52:37
:tags: Card
:category: none

.. raw:: html

  <div itemscope itemtype="://schema.org/Organization">
  <h3 itemprop="name">Logilab</h3>
  <div style="display:none" itemprop="description">Logilab développe des solutions logicielles,  et propose des services et des formations de haut niveau dans les domaines de l'informatique scientifique et de la gestion de connaissances, en utilisant des logiciels libres et méthodes agiles.</div>
    Tél :<span itemprop="telephone"> (+33) 1 45 32 03 12</span><br>
    <span itemprop="email">contact@logilab.fr</span><br> 
    <a itemprop="url" href="://www.logilab.fr">://www.logilab.fr/</a>
  <div itemprop="department"><strong itemprop="name">Formations</strong> : <span itemprop="telephone"> (+33) 1 47 07 68 25</span> / <span itemprop="email">formation@logilab.fr</div>
  <div><img src="://www.logilab.fr/file/1874?vid=download"></div>
  <h3>Paris</h3>
  <div itemprop="address" itemscope itemtype="://schema.org/PostalAddress">
    <span itemprop="streetAddress">104 boulevard Louis-Auguste Blanqui</span><br>
    <span itemprop="postalCode">75013</span>, 
    <span itemprop="addressLocality">Paris</span>, 
    <span itemprop="addressRegion">Ile-de-France</span><br>
    <span itemprop="addressCountry">France</span><br>
    Métro : Glacière (ligne 6)<br>
    Tél :<span itemprop="telephone"> (+33) 1 45 32 03 12</span><br>
   </div>
  </div>

..  <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="://www.openstreetmap.org/export/embed.html?bbox=2.2587,48.8179,2.4208,48.9011&amp;layer=mapquest&amp;marker=48.82992,2.34524" style="border: 1px solid black"></iframe><br /><small><a href="://www.openstreetmap.org/?lat=48.8595&amp;lon=2.339749999999981&amp;zoom=12&amp;layers=Q&amp;mlat=48.82992&amp;mlon=2.34524">Voir une carte plus grande</a></small>

.. raw:: html

 <div xmlns="://www.w3.org/1999/xhtml"
  xmlns:rdf="://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rdfs="://www.w3.org/2000/01/rdf-schema#"
  xmlns:xsd="://www.w3.org/2001/XMLSchema#"
  xmlns:gr="://purl.org/goodrelations/v1#"
  xmlns:foaf="://xmlns.com/foaf/0.1/"
  xmlns:vcard="://www.w3.org/2006/vcard/ns#">
 
  <div typeof="gr:BusinessEntity" about="#company">
    <div property="gr:legalName" content="Logilab"></div>
    <div property="vcard:tel" content="+33-145320312"></div>
    <div rel="vcard:adr">
      <div typeof="vcard:Address">
        <div property="vcard:country-name" content="France"></div>
        <div property="vcard:locality" content="Paris"></div>
        <div property="vcard:postal-code" content="75013"></div>
        <div property="vcard:street-address" content="104 Boulevard Louis-Auguste Blanqui"></div>
      </div>
    </div>
    <div rel="foaf:logo" resource="://www.logilab.fr/data/logo.png"></div>
    <div rel="foaf:page" resource=""></div>
  </div>
 </div>

.. raw:: html

  <div itemprop="address" itemscope itemtype="://schema.org/PostalAddress">
    <span style="display:none" itemprop="name">Logilab</span>
    <h3>Toulouse</h3>
    <span itemprop="streetAddress">25 allée Jean Jaurès</span><br>
    <span itemprop="postalCode">31000</span>, 
    <span itemprop="addressLocality">Toulouse</span>, 
    <span itemprop="addressRegion">Midi-pyrénées</span><br>
    <span itemprop="addressCountry">France</span><br>
    Métro : Jean-Jaurès (lignes A, B)<br>
    Tél :<span itemprop="telephone"> (+33) 5 62 17 16 42</span><br>
 </div>

=============================
 Nantes 
=============================

Pas de bureaux ouverts au public.

Tél : (+33) 1 45 32 03 12

=============================
 Valence
=============================

Pas de bureaux ouverts au public.

Tél : (+33) 1 45 32 03 12

