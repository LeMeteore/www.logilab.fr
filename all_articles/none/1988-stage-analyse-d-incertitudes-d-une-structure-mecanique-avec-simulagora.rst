
Stage - Analyse d'incertitudes d'une structure mécanique avec Simulagora
########################################################################


:slug: stage-analyse-d-incertitudes-d-une-structure-mecanique-avec-simulagora
:date: 2014/01/08 09:44:27
:tags: Card
:category: none

Contexte
========

L'arrivée des nuages de serveurs (*cloud computing*) a un impact fort sur le
calcul scientifique. En effet, de grandes structures, comme Amazon ou Google aux
États-Unis, mettent aujourd'hui à disposition sur Internet des serveurs facturés
à l'usage et permettent à tout le monde d'avoir accès à une puissance de calcul
qui semble illimitée en regard de la plupart des usages. Dans le domaine
scientifique, cela signifie que des simulations qui nécessitent ponctuellement
des ressources très importantes sont désormais possibles.

Logilab propose aux ingénieurs et chercheurs une plateforme de simulation
numérique qui facilite la mise au point, l'archivage et la gestion de cas de
calcul. Elle permet notamment de mettre en œuvre des études paramétriques en
utilisant les ressources virtuellement illimitées proposées par les
gestionnaires de nuages informatiques (Amazon Web Services, Google Compute,
etc.)

Problématique
=============

L'objectif du stage est de disposer d'un exemple démontrant les avantages de la
plateforme Simulagora_ pour le calcul scientifique dans le domaine de la
mécanique. Le stage consistera à mener une analyse d'incertitudes sur une
structure mécanique en utilisant un modèle réduit stochastique en synthèse
modale.

Cette analyse a pour objet d'étudier l'influence des variations des
propriétés mécaniques d'une structure complexe sur sa réponse
dynamique (réponse forcée à une excitation périodique par
exemple). Les incertitudes seront quantifiées dans un espace modal,
plutôt que dans un espace paramétrique (par exemple : géométrie,
matériaux, tolérance, etc.). Ce choix est motivé par deux raisons
principales : tout d'abord une meilleure accessibilité via des mesures
expérimentales, et ensuite un espace paramétrique plus réduit.

D'autre part, la structure étudiée est supposée suffisament complexe
pour justifier une modélisation par sous-structuration (c'est-à-dire
qu'on la considère constituée d'un ensemble de composants
inter-connectés). Ceci permet d'une part de distribuer les coûts de
calcul et d'autre part d'introduire les incertitudes au niveau de ces
composants plutôt qu'au niveau de la structure globale. Ceci amène
naturellement à la construction d'un modèle réduit en synthèse modale,
modèle dans lequel on introduit les incertitudes par perturbation (on
parle de modèle réduit stochastique).

Une simple simulation de Monte-Carlo ou éventuellement des analyses
d'optimisation robuste permettront d'obtenir au final les réponses
attendues.

Rôle
====

Intégré à l'équipe de recherche et développement "Simulation
Numérique" de Logilab, sous la tutelle d'un ingénieur expert en
informatique scientifique ayant des compétences poussées en mécanique,
vous l'assisterez dans son travail quotidien et pourrez être amené à
effectuer tout ou partie des travaux suivants, en collaboration avec
l'équipe :

* concevoir une analyse d'incertitudes d'une structure mécanique en
  utilisant un modèle réduit stochastique en synthèse modale ;

* mettre en place cette analyse dans la plateforme Simulagora en
  s'appuyant sur le code de simulation thermomécanique `Code_Aster`_ ;

* rédiger un document dynamique présentant l'analyse, permettant de
  la lancer et d'en afficher les résultats.


Compétences attendues
=====================

* Excellentes connaissances en simulation numérique et en dynamique
  des structures
* Une expérience d'utilisation de `Code_Aster`_ sera appréciée
* Intérêt pour les technologies du Web
* Anglais technique


Niveau
======

Bac+5 (Master 2 ; 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.



.. _Python: ://www.python.org/
.. _Simulagora: ://www.simulagora.com
.. _`Code_Aster`: ://www.code-aster.org

