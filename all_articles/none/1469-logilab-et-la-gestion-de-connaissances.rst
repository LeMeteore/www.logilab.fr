
Logilab et la gestion de connaissances
######################################


:slug: logilab-et-la-gestion-de-connaissances
:date: 2011/12/01 15:52:37
:tags: Card
:category: none

`CubicWeb <://www.cubicweb.org/>`_  permet de déployer rapidement des applications Web de gestion de connaissances, à partir du schéma des données manipulées.

L'interface utilisateur de CubicWeb a été spécialement conçue pour laisser à l'utilisateur final toute latitude pour sélectionner puis    présenter les données. Elle permet d'explorer aisément la base de connaissances et d'afficher les résultats avec la présentation la mieux    adaptée à la tâche en cours. La flexibilité de cette interface redonne à l'utilisateur le contrôle de paramètres d'affichage et de présentation qui sont habituellement réservés aux développeurs.

Parmi les applications déjà réalisées, on dénombre la fusion et la publication de plusieurs catalogues de la Bibliothèque nationale de France (voir ://data.bnf.fr/), un système de gestion d'études numériques et de simulations pour un bureau d'études, la mise en ligne des collections des musées de Haute-Normandie (voir ://www.musees-haute-normandie.fr/collections/), la gestion des compétences du personnel et de l'historique des projets d'une entreprise d'ingénierie, la gestion du développement de projets logiciels d'un éditeur, la gestion de la scolarité d'un institut de formation, un intranet de travail collaboratif, la gestion de la relation client,    etc.

://www.cubicweb.org/

