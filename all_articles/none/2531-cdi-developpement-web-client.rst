
CDI - DÉVELOPPEMENT WEB (client)
################################


:slug: cdi-developpement-web-client
:date: 2016/02/22 15:18:51
:tags: Card
:category: none

Vous êtes passionné(e) d'informatique et souhaitez comprendre et  maîtriser le fonctionnement de toute la pile
applicative, de la base de données à la feuille de style, pour concevoir et développer des produits avec agilité ?
Nous aussi !

D'abord précurseur dans les années 2000, nous sommes aujourd'hui le spécialiste français des projets d'envergure dans le domaine du web sémantique et des données ouvertes. Nous aimons coder et réaliser de beaux produits, le logiciel libre est une des bases de notre culture, nous pratiquons les méthodes agiles et aspirons à une entreprise libérée.

Si vous partagez ces valeurs, rejoignez-nous !

Mission
-------

Intégré(e) à l'équipe de développement, vous participerez à la réalisation de projets logiciels ayant trait au web sémantique.

Profil
------

H/F - BAC+5 ou supérieur.

Expérience
----------

Profils expérimentés préférés, mais débutant(e)s accepté(e)s.

Compétences / Connaissances souhaitées
--------------------------------------

* Programmation avec JavaScript (le TC39, CanIUse et Babel sont nos amis) en utilisant les bibliothèques les plus adaptées au projet (vous préférez lesquelles, vous ?)

* Programmation avec Python en environnement Unix/Linux

* Web : HTML(5) et CSS(3), HTTP(S), REST, HATEOAS, etc.

* Web sémantique : RDF, OWL, SPARQL, JSON-LD, API hypermedia, etc.

* Utilisation d'un gestionnaire de version distribué: hg, git.

* Modélisation de données et conception logicielle (UML).

* Méthodes agiles d'organisation (XP, SCRUM, etc.)

* Architecture de système d'information (Bases de données, client-serveur, réseau, objets distribués, etc.).

* Autonomie et capacité à travailler en équipe.

* Anglais.

* Habitude du logiciel libre.

Durée
------

Contrat à Durée Indéterminée.

Lieu
----
Paris (75013) ou Toulouse.

Rémunération
-------------
Rémunération selon le profil.

Candidatures
-------------
Envoyez votre candidature (CV + lettre de motivation, format PDF ou HTML) par courrier électronique à personnel@logilab.fr.

