
Logilab et Zope / Plone / CPS
#############################


:slug: logilab-et-zope-plone-cps
:date: 2011/12/01 15:52:37
:tags: Card
:category: none

Zope / Plone / CPS sont des systèmes de gestion de contenu couramment utilisés il y a quelques années pour construire des plates-formes Web. **Logilab** s'est appuyée sur ces systèmes pour développer différentes applications Web, différents intranets et différents portails collaboratifs de gestion de contenu. Elle a également participé activement à la communauté Logiciel Libre autour de ces outils.

**Logilab** travaillait sur ces projets en partenariat avec des agences graphiques et se chargeait de tous les aspects techniques, depuis le conseil quant au choix du système de gestion de contenu jusqu'à la mise en œuvre de la charte graphique, en passant par la sélection des composants sous licence libre et le développement des extensions manquantes.

Aujourd'hui, **Logilab** continue à maintenir des applications développés avec ces outils mais privilégie l'utilisation d'outils plus récents et plus puissants pour développer de nouvelles applications (`en savoir plus </web-semantique>`_).

