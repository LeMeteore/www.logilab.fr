
Formations
##########


:slug: formations
:date: 2011/12/01 15:52:37
:tags: Card
:category: none

.. This content is generated and should not be edited directly


Préface
=======

Assister à une formation Logilab, c'est : 


- apprendre avec des professionnels compétents maîtrisant parfaitement le sujet qu'ils enseignent,


- bénéficier des conseils et des avis d'experts travaillant depuis longtemps dans le domaine,


- trouver des interlocuteurs attentifs et disponibles, cherchant à apporter à chacun les informations dont il a besoin.




Les experts du centre de compétences **Logilab** participent au développement de solutions logicielles telles que **CubicWeb** ou **Narval**, mettant en œuvre des fonctionnalités et des algorithmes complexes. Pour ce faire, ils utilisent des langages tels que Python et C++, des méthodes telles que *eXtreme Programming* ou le processus unifié, etc. Confrontés quotidiennement à ces domaines de connaissance, ils ont acquis une réelle expertise sur ces sujets, ce qui leur offre la possibilité de les enseigner dans un esprit pragmatique. Les experts choisis pour animer une formation ont tous, outre leur excellence technique, une expérience notable dans le domaine de la formation professionnelle. La majorité d'entre eux enseigne pour le compte de grands organismes internationaux de formation professionnelle en informatique et est reconnue pour son savoir-faire pédagogique. Certains enseignent aussi dans des universités ou des écoles d'ingénieurs (ECP, ENSTA, ESIEE, ...).

Référencement officiel
-----------------------

Logilab est déclaré comme organisme de formation auprès de la `Direccte <://direccte.gouv.fr/>`_ sous le numéro 117.534.173.75 et référencé dans `Data-Dock <https://www.data-dock.fr/>`_ avec l'identifiant 0017742.

Catalogue
---------

Toutes nos formations sont disponibles soit en langue française soit en langue anglaise. Elles peuvent toutes avoir lieu en intra-entreprise au lieu de votre convenance en France ou en Europe ; des sessions en inter-entreprises sont organisées par **Logilab** pour certaines formations en région Île-de-France ou en région Midi-Pyrénées. L'agenda des sessions de formation est  `disponible au format iCal <://www.logilab.fr/event?vid=ical>`_.

**Logilab** a choisi de systématiquement adapter ses formations au plus près des besoins de chacun de ses clients. Ainsi, vous pouvez soit choisir une de nos formations standards décrites ci-après, soit contacter notre  `service commercial <://www.logilab.fr/contact>`_ pour composer une formation sur-mesure. Nous disposons d'une importante bibliothèque de modules pédagogiques pour vous aider dans cette opération et fabriquer la formation répondant exactement à vos besoins.

Notre catalogue est disponible au  `format PDF <://www.logilab.fr/publications/catalogue-formations.pdf>`_.


Formations standards
--------------------

:rql:`Any T,TN ORDERBY TO WHERE T name TN, T order TO, EXISTS(X filed_under T, X in_state S, S name "training_published"):logilabfr.training.bytheme`


Formation à la carte
--------------------

À partir de 4 personnes, **Logilab** peut animer des formations spécifiques en intra-entreprise, n'importe où en France ou en Europe. Vous pouvez alors composer la formation répondant exactement à vos besoins. Notre  `service commercial <://www.logilab.fr/contact>`_ est à votre disposition pour vous conseiller et vous aider dans vos choix, en s'appuyant sur l'importante bibliothèque de modules pédagogiques dont nous disposons.


..
  Formations DIF
  --------------

  Depuis la mise en place du droit individuel à la formation, **Logilab** propose une offre spécifique pour les personnes souhaitant consacrer leur crédit formation à l'entretien ou l'amélioration de leurs compétences informatiques. Ces formations reprennent les thèmes ci-dessus, mais sont organisées sur des périodes plus courtes (de préférence trois jours) et peuvent se dérouler pour partie en alternance et à distance, de manière à s'adapter au mieux aux contraintes d'emploi du temps et de budget des participants. Notre  `service commercial <://www.logilab.fr/contact>`_ est à votre disposition pour vous présenter en détail cette offre.


Commercialisation
-----------------

L'équipe commerciale de **Logilab** est à votre disposition pour vous apporter toute précision supplémentaire, vous aider à formaliser vos besoins et vous proposer une prestation adaptée à vos besoins.

