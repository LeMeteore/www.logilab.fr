
Denis Laxalde
#############


:slug: denis-laxalde
:date: 2014/07/07 08:40:00
:tags: Card
:category: none

* courriel : `denis.laxalde@logilab.fr <mailto:denis.laxalde@logilab.fr>`_
* messagerie instantanée jabber/xmpp : `dlaxalde@jabber.logilab.org <xmpp:dlaxalde@jabber.logilab.org>`_ et `salon public@conference.jabber.logilab.org <xmpp:public@conference.jabber.logilab.org?join>`_
* téléphone : +33 5 62 17 16 42
* `github <https://github.com/dlax>`_
* `bitbucket <https://bitbucket.org/dlax>`_
* foaf : `logilab.org <://www.logilab.org/cwuser/dlaxalde?vid=foaf>`_, `cubicweb.org <://www.cubicweb.org/cwuser/dlaxalde?vid=foaf>`_

Télécharger le contact en format vcard (à venir)

