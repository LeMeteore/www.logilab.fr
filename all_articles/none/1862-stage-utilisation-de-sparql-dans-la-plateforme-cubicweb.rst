
Stage  - Utilisation de SPARQL dans la plateforme CubicWeb
##########################################################


:slug: stage-utilisation-de-sparql-dans-la-plateforme-cubicweb
:date: 2012/11/13 10:57:13
:tags: Card
:category: none

Contexte
========

CubicWeb_ est une plateforme logicielle libre permettant de développer
rapidement des applications de gestion de données. Elle s'appuie
fortement sur le modèle métier des informations gérées pour proposer
une approche originale laissant à l'utilisateur final toute latitude
pour naviguer dans le graphe des informations, sélectionner des
données et choisir la vue qui lui permettra de les visualiser. De son
côté, le développeur dispose de fonctionnalités automatisant, à partir
du modèle métier, nombre de tâches habituelles (gestion de
la base relationnelle sous-jacente, génération d'une interface Web par
défaut, migration entre versions successives, etc.)

.. _CubicWeb: ://www.cubicweb.org/

Problématique
=============

CubicWeb_ est aujourd'hui utilisé pour publier sur Internet de très
grosses bases de données, qu'il s'agisse de catalogues de
bibliothèques nationales ou d'images de l'activité cérébrale utilisées
pour la recherche médicale. Il est déjà possible d'utiliser le
standard SPARQL_ pour interroger une base CubicWeb_, mais toutes les
fonctionnalités de SPARQL_ ne sont pas gérées.

.. _SPARQL: ://www.w3.org/TR/rdf-sparql-query/

L'objectif de ce stage sera d'étudier, et si possible réaliser, la
mise en œuvre complète du standard SPARQL_ 1.1 dans CubicWeb_.

Rôle
====

Intégré à l'équipe de recherche et développement "Web Sémantique" de
Logilab, sous la tutelle d'un ingénieur spécialiste de la gestion de
données, vous l'assisterez dans son travail quotidien et pourrez être
amené à effectuer tout ou partie des travaux suivants, en
collaboration avec l'équipe :

* développer dans la plateforme CubicWeb_ des fonctionnalités
  permettant l'interrogation en SPARQL_,

* travailler à la traduction semi-automatique entre requête SQL et requête
  SPARQL_ utilisant des vocabulaires externes standards (dbpedia_, geonames_,
  freebase_, etc.)

.. _dbpedia: ://dbpedia.org/
.. _geonames: ://www.geonames.org/
.. _freebase: ://www.freebase.com/

Compétences attendues
=====================

* Bonnes connaissances pratiques en programmation objet,
* Connaissance des bases de données (idéalement Postgresql_),
* Connaissance des standards du Web Sémantique,
* Anglais technique.
* La connaissance du langage Python_ sera appréciée.

.. _Python: ://www.python.org/
.. _Postgresql: ://www.postgresql.org/

Niveau
======

Bac+4/5 (Master 1 ou 2 ; 2ème ou 3ème année d'école d'ingénieur)

Durée
=====

6 mois

Rémunération
============

Ce stage fait l'objet d'une rémunération, variable selon le niveau d'études.

Candidatures
============

Envoyez votre candidature (CV + lettre de motivation, format PDF ou
HTML) par courrier électronique à personnel@logilab.fr.

