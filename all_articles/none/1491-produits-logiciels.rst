
Produits logiciels
##################


:slug: produits-logiciels
:date: 2011/12/01 15:52:37
:tags: Card
:category: none

**Logilab** mène tous ses projets dans l'idée que la machine doit         décharger l'utilisateur des tâches les plus simples ou les plus         répétitives pour lui permettre de se concentrer sur ce qui demande         réflexion.

Entre autres, sont disponibles :

    * `CubicWeb <://www.cubicweb.org/>`_ - application générique de gestion des connaissances,        pensée pour faciliter la gestion mais surtout la recherche et la navigation au travers d'une base de connaissances,

    * `pylint <://www.pylint.org>`_ - outil d'analyse statique de code qui permet de vérifier la viabilité du code Python produit et le respect des standards de codage,

    * `hgview  <://www.hgview.org>`_ - application de visualisation de code source stocké dans un dépot de gestion de source mercurial. 

Enfin, certains logiciels sont disponibles sous licence GPL sur les sites Webs des projets Logiciel Libre de **Logilab**.

L'équipe commerciale de Logilab est à votre disposition pour vous apporter toute précision supplémentaire ou pour envisager avec vous des adaptations ou développements spécifiques.

