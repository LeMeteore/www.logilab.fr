
Logilab participe à Agile Tour Nantes
#####################################


:slug: logilab-participe-a-agile-tour-nantes
:date: 2012/11/26 12:17:35
:tags: BlogEntry
:category: Évènements

À l'`Agile Tour Nantes <://at2012.agiletour.org/fr/nantes.html>`_ nous avons présenté "Outils agiles : revue de code & publication continue", expliquant l'utilisation de la plate-forme logiciel libre `CubicWeb <://www.cubicweb.org>`_ pour la revue de code collaborative et la publication automatisée de paquets installables. Les transparents sont disponibles en `PDF <://www.logilab.org/file/111231?vid=download>`_ (et en miroir sur `slideshare <://fr.slideshare.net/arthurlutz/prsentation-outils-agiles-revue-de-code-publication-continue>`_). Merci à `Agile Nantes <://agilenantes.org/>`_ pour l'organisation de cette étape du Tour.

