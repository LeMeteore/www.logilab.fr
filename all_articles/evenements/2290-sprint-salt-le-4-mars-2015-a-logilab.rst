
Sprint Salt le 4 mars 2015 à Logilab
####################################


:slug: sprint-salt-le-4-mars-2015-a-logilab
:date: 2015/02/24 17:08:04
:tags: BlogEntry
:category: Évènements

En marge de la conférence SaltStack, nous vous invitons à un `sprint SaltStack mercredi 4 mars 2015 <://lists.afpy.org/pipermail/salt-fr/2015-February/000119.html>`_ de 9h à 18h dans nos locaux à Paris. Voir aussi l'`annonce sur le blog de la communauté française <://salt-fr.afpy.org/sprint-salt-en-marge-de-la-saltconf.html>`_. 

L'idée est de finir la journée avec des contributions à salt (documentation, code, tests, etc) et de profiter des pauses pour discuter technique et échanger sur nos pratiques.

La participation est gratuite, il suffit de s'inscrire `ici <https://framadate.org/z7mvrnwcxmc7o3l4>`_.

