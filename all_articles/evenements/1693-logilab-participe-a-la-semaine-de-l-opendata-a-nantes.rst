
Logilab participe à la semaine de l'OpenData à Nantes
#####################################################


:slug: logilab-participe-a-la-semaine-de-l-opendata-a-nantes
:date: 2012/05/21 14:35:42
:tags: BlogEntry
:category: Évènements

Logilab fera une `présentation du Web Sémantique <://www.logilab.org/file/93162?vid=download>`_ lors la `Semaine Open Data <://www.opendataweek.org/>`_ à Nantes. Cette présentation aura lieu lors des `atelier participatifs <://www.opendataweek.org/?page_id=2>`_, et l'inscription s’effectue sur `eventbrite <://www.eventbrite.com/event/3400604299?ref=ebtn>`_.

