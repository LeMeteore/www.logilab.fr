
Logilab fête ses 15 ans !
#########################


:slug: logilab-fete-ses-15-ans
:date: 2015/09/08 12:30:55
:tags: BlogEntry
:category: Actualités

Venez fêter les 15 ans de Logilab avec nous !

Envoyez un courrier électronique à 15ans@logilab.fr pour recevoir votre invitation !

Les amis des amis du Logiciel Libre sont les amis de Logilab,
venez accompagné, mais confirmation préalable appréciée.

Suivez nos actualités :

- `Twitter <https://twitter.com/logilab>`_
- `LinkedIn <https://www.linkedin.com/company/logilab>`_
- `SlideShare <://fr.slideshare.net/logilab>`_
- `Le blog Logilab <https://www.logilab.fr/blog/1377>`_

