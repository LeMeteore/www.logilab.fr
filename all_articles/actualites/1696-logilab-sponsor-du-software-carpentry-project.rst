
Logilab sponsor du Software Carpentry Project
#############################################


:slug: logilab-sponsor-du-software-carpentry-project
:date: 2012/05/25 21:20:53
:tags: BlogEntry
:category: Actualités

Logilab sponsorise l'atelier_ de formation à la conception de logiciel et à la gestion de version, qui aura lieu à l'INRIA à Paris les 28 et 29 juin et s'adresse aux scientifiques et ingénieurs.

.. _atelier: ://software-carpentry.org/boot-camps/paris-june-2012/

