
Soirée Salt à Paris le 12 mai 2016
##################################


:slug: soiree-salt-a-paris-le-12-mai-2016
:date: 2016/04/27 18:03:28
:tags: BlogEntry
:category: Actualités

En que partenaire SaltStack, nous animons la communauté dynamique autour du logiciel libre Salt.

**À ce titre, nous vous invitons au prochain meet-up Salt**

.. image :: ./images/"meetupSalt.png"

À cette occasion, deux présentations seront faites :

- `Séven Le Mesle <https://twitter.com/slemesle>`_ de WeScale expliquera comment Salt et Docker peuvent être utilisés pour réaliser des tests d'intégration ;

- `Arthur Lutz <https://twitter.com/arthurlutz>`_ de Logilab présentera comment utiliser Sentry pour collecter les logs, retours et erreurs sur une infrastructure pilotée par Salt.

`INSCRIVEZ-VOUS <://www.meetup.com/fr-FR/Paris-Salt-Meetup/events/230682687/?eventId=230682687>`_

WeScale : 156 boulevard Haussmann 75009 Paris.

