
Atelier AFNOR usages du Web pour l'industrie
############################################


:slug: atelier-afnor-usages-du-web-pour-l-industrie
:date: 2017/05/11 18:18:36
:tags: BlogEntry
:category: Actualités

Logilab présentera sa vision de l'utilisation des standards du Web dans l'industrie lors du prochain `atelier AFNOR <https://www.linkedin.com/pulse/atelier-ouvert-mod%C3%A8les-de-donn%C3%A9es-internet-pour-18-philippe>`_ sur le sujet le 18 mai 2017.

