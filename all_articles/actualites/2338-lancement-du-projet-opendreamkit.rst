
Lancement du projet OpenDreamKit
################################


:slug: lancement-du-projet-opendreamkit
:date: 2015/09/02 11:09:45
:tags: BlogEntry
:category: Actualités

OpenDreamKit est un projet européen Horizon 2020 qui se déroulera pendant quatre ans, à partir de Septembre 2015. Ce projet permettra d'améliorer les outils logiciel libre pour la recherche collaborative en mathématiques fondamentales, par exemple SageMath, IPython / Jupyter, Pythran, etc.

Florent Cayré, directeur du département informatique scientifique au sein de Logilab, est présent pendant 3 jours à Orsay pour le lancement du projet.

