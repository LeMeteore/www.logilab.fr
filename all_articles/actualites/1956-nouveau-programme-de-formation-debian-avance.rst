
Nouveau programme de formation Debian avancé
############################################


:slug: nouveau-programme-de-formation-debian-avance
:date: 2013/12/05 17:54:24
:tags: BlogEntry
:category: Actualités

Le programme de notre formation pour futur gourou administrateur système a été entièrement revu et corrigé pour être à la pointe des technologies actuelles.

`Au programme`_, entre autres : virtualisation, netboot, approvisionnement, gestion de configuration...

Et comme une bonne nouvelle n'arrive jamais seule, une session inter-entreprises de cette formation sera programmée au 1er semestre 2014.

.. image:: ://www.debian.org/Pics/joy_web_logo.png

.. _`Au programme`: ://www.logilab.fr/formations/debian-admin-avance

