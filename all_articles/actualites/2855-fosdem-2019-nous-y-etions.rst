
Fosdem 2019, nous y étions !
############################


:slug: fosdem-2019-nous-y-etions
:date: 2019/02/19 17:03:45
:tags: BlogEntry
:category: Actualités

Découvrez `le retour de Nicolas Chauvat <https://www.logilab.org/blogentry/10131142>`_ sur l'édition 2019 du FOSDEM qui a lieu les 2 et 3 février à Bruxelles, en Belgique.

.. image :: ./images/"FOSDEM.jpeg"

À cette occasion, Nicolas a présenté le dernier projet de Logilab, un redémarrage de CubicWeb pour le transformer en une `extension Web permettant de parcourir le Web des données <https://fosdem.org/2019/schedule/event/collab_cwldbe/>`_. L'enregistrement de la conférence, les diapositives et la vidéo de la démo sont en ligne.

