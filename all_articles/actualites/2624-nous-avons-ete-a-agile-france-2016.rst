
Nous avons été à Agile France 2016 !
####################################


:slug: nous-avons-ete-a-agile-france-2016
:date: 2016/06/30 18:47:58
:tags: BlogEntry
:category: Actualités

Notre équipe était présente à la conférence `Agile France <://2016.conf.agile-france.org/>`_ qui a lieu les 16 et 17 juin au Chalet de la Porte Jaune, à Paris.

.. image :: ./images/"AgileFrance.png"

`Découvrez notre retour ! <https://www.logilab.org/6833982>`_

