
Logilab au congrès Nafems 2012
##############################


:slug: logilab-au-congres-nafems-2012
:date: 2012/05/30 15:44:26
:tags: BlogEntry
:category: Actualités

Logilab présentera au congrès Nafems_ les 6 et 7 juin 2012 à Paris son offre cloud/SaaS pour l'exécution de calculs paramétriques de simulation numérique. Retrouvez-nous sur notre stand.

.. _Nafems: ://www.nafems.org/events/nafems/2012/congres_nafems_france_2012/

