
Logilab présent à Paris Open Source Summit
##########################################


:slug: logilab-present-a-paris-open-source-summit
:date: 2018/12/04 16:32:29
:tags: BlogEntry
:category: Actualités

.. image :: ./images/"bandeau_slide-UK-2018-OK2 (1).png"

**Retrouvez-nous au stand C6-D5 du salon**

5 & 6 décembre au Dock Pullman, plaine Saint-Denis

Nous vous accueillerons avec plaisir au salon Paris Open Source Summit pour parler logiciel libre, données ouvertes et Web sémantique.

Et ne ratez pas la présentation de `Arthur Lutz <https://twitter.com/arthurlutz>`_ : **Retour d'expérience sur la mise en place de déploiement continu** qui aura lieu dans la matinée du mercredi 5 décembre, track Devops.


`Demandez votre badge d'accès gratuit ! <https://www.opensourcesummit.paris/preinscription_154_204_p.html>`_

