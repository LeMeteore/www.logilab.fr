
Présentation conjointe Logilab / SNCF au séminaire NAFEMS
#########################################################


:slug: presentation-conjointe-logilab-sncf-au-seminaire-nafems
:date: 2012/11/26 12:25:57
:tags: BlogEntry
:category: Actualités

Logilab et la SNCF effectueront ensemble une présentation intitulée  "Développement d’une démarche Simulation Data Management (SDM) dédiée à l’infrastructure ferroviaire" lors du séminaire `NAFEMS <://nafems.org/>`_ du `6 décembre 2012 <://www.nafems.org/events/nafems/2012/page31/>`_\ . Cette présentation sera l'occasion d'exposer la démarche de capitalisation mise en place par la SNCF, et l'outil de SDM développé par Logilab sur la base de `CubicWeb <://www.cubicweb.org/>`_\ .

