
Le site intitutionnel de Logilab fait peau neuve
################################################


:slug: le-site-intitutionnel-de-logilab-fait-peau-neuve
:date: 2012/01/06 09:30:36
:tags: BlogEntry
:category: Actualités

Nouvelle charte graphique, nouvel outil de publication... et
beaucoup de réglages et de corrections de coquilles à faire dans
les semaines qui viennent !

Très rapidement, un site ://www.logilab.fr/ plus dynamique et proposant plus de contenu.

