
Logilab présent au PyConFr 2018 !
#################################


:slug: logilab-present-au-pyconfr-2018
:date: 2018/10/01 17:37:15
:tags: BlogEntry
:category: Actualités

Pionnier du langage Python en France, **Logilab** est mécène de `PyConFr <https://www.pycon.fr/2018/>`_, conférence annuelle des pythonistes francophones qui aura lieu du jeudi 4 au dimanche 7 octobre, à Lille.

.. image :: ./images/"PyConFr2018.png"

Un codage participatif aura lieu les jeudi 4 et vendredi 5 octobre. Des développeuses et des développeurs de différents projets *open source* se rassembleront pour coder ensemble.
C'est l'occasion de participer au développement de son projet préféré !


Durant le week-end, auront lieu des présentations sur des sujets variés, autour du **langage Python**, de ses usages, des bonnes pratiques, des retours d'expériences, des partages d'idées.

Cette année, deux ingénieurs de notre équipe sont au programme :

- `Arthur Lutz <https://twitter.com/arthurlutz>`_ présentera **Déployer des applications python dans un cluster openshift** et aussi **Faire de la domotique libriste avec Python**

Et

- Julien Tayon présentera **La cartographie c'est simple et "complexe"** 



La billetterie en ligne est fermée ! Pour plus d'informations, rapprochez-vous de l'`association organisatrice <https://www.helloasso.com/associations/afpy>`_.

