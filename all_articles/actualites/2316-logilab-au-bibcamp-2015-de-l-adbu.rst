
Logilab au Bibcamp 2015 de l'ADBU
#################################


:slug: logilab-au-bibcamp-2015-de-l-adbu
:date: 2015/06/26 10:15:28
:tags: BlogEntry
:category: Actualités

Logilab a participé au `Bibcamp 2015 de l'ADBU <://adbu.fr/bibcamp-numerique-an-ii-dessine-moi-un-systeme-dinformation-lyon-les-22-et-23-juin-2015/>`_ à Lyon pour y présenter `Innover par et pour les données </file/2315/raw>`_ et réfléchir avec les autres personnes présentes aux changements à venir dans les bibliothèques universitaires.

Merci aux organisateurs et aux très agréables participants pour ces journées intéressantes et riches d'enseignements.

.. image:: ://www.adbu.fr/competplug/uploads/2015/02/adbu.png

La présentation est aussi `consultable sur slideshare <://fr.slideshare.net/logilab/logilab-adbubibcamp2015>`_

