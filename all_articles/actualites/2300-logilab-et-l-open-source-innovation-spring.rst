
Logilab et l'Open Source Innovation Spring
##########################################


:slug: logilab-et-l-open-source-innovation-spring
:date: 2015/04/01 15:55:50
:tags: BlogEntry
:category: Actualités

Logilab participe activement à l'organisation de l'`Open Source
Innovation Spring <://www.open-source-innovation-spring.org/>`_,
tant pour les aspects matériels que pour la mise au point du programme
des sessions.

.. image:: ://www.logilab.fr/file/2296/raw/OpenSourceInnovationSpring_Systematic-GTLogicielLibre.png
   :align: center

Ce jeudi 2 avril à 15h10, `David Douard`_, directeur du département
Outils & Systèmes de Logilab, interviendra au cours de la session
`[Cloud] Conteneurs open source (Docker...)`_ pour expliquer comment
"initialiser des conteneurs Docker à partir de configurations Salt
construites à plusieurs grâce à Mercurial".

Cette session aura lieu dans les prestigieux `locaux parisiens de
Mozilla <https://wiki.mozilla.org/Paris>`_.  L'accès est gratuit, mais
`l'inscription est obligatoire`_.

Salt est un environnement d'exécution distribué et asynchrone, écrit
en Python, qui se positionne comme le couteau suisse de la gestion
d'infrastructure. Logilab est le partenaire français de SaltStack Inc.

Ce vendredi 3 avril de 8h30 à 19h, une partie de l'équipe
`Informatique Scientifique
<https://www.logilab.fr/informatique-scientifique>`_ sera présente à
la journée `PyData <://pydataparis.joinux.org/>`_ centrée sur les
usages de Python pour le traitement de données.

Logilab soutient PyData en tant que sponsor et co-organisateur,
`inscrivez-vous <https://www.weezevent.com/pydata-paris>`_ avec le
code promo SEMWEBPRO pour bénéficier d'une réduction de 20%.

.. _`David Douard`: ://www.logilab.fr/id/david.douard
.. _`[Cloud] Conteneurs open source (Docker...)`: ://www.open-source-innovation-spring.org
.. _`l'inscription est obligatoire`: https://www.eventbrite.com/e/les-conteneurs-open-source-docker-tickets-15757635512

