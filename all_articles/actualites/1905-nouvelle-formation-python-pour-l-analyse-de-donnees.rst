
Nouvelle formation "Python pour l'analyse de données"
#####################################################


:slug: nouvelle-formation-python-pour-l-analyse-de-donnees
:date: 2013/05/16 22:07:48
:tags: BlogEntry
:category: Actualités

Une `nouvelle formation`_ à destination des personnes souhaitant utiliser Python pour faire de l'analyse de données, indépendamment du domaine d'application, vient d'être ajoutée à notre catalogue.

Sur le modèle de notre `formation d'introduction à Python pour le scientifique`_, une connaissance préalable de Python n'est pas nécessaire.

Cette nouvelle formation sera disponible en inter-entreprises comme en intra-entreprise.

.. _`nouvelle formation`: ://www.logilab.fr/formations/python-num-data

.. _`formation d'introduction à Python pour le scientifique`: https://www.logilab.fr/formations/python-num-intro

