
Logilab soutient pgDay Paris 2019
#################################


:slug: logilab-soutient-pgday-paris-2019
:date: 2019/03/05 17:33:19
:tags: BlogEntry
:category: Actualités

Nous avons le plaisir de soutenir l'organisation du `pgDay Paris 2019 <https://2019.pgday.paris/>`_ aux côté des spécialistes de Postgresql et d'autres sociétés qui en font un usage intensif.

Consulter le programme, il est montre que Postgresql est une base de données très flexible, qui allie performances et très grandes quantités de données !

