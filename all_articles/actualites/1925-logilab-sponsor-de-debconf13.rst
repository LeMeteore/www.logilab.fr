
Logilab sponsor de DebConf13
############################


:slug: logilab-sponsor-de-debconf13
:date: 2013/08/26 00:00:00
:tags: BlogEntry
:category: Actualités

Logilab a participé à DebConf13_, la conférence annuelle de la distribution Debian_: en y envoyant ses *Debian Developers*, en étant sponsor de l'événement et en effectuant un don. Lisez le `compte-rendu`_ de la semaine.


.. _Debian: ://www.debian.org/
.. _DebConf13: ://debconf13.debconf.org/
.. _`compte-rendu`: ://www.logilab.org/blogentry/179176

.. image :: ://debconf13.debconf.org/images/logo.png
   :align: center

