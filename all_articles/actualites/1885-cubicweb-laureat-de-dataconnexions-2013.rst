
CubicWeb lauréat de Dataconnexions 2013
#######################################


:slug: cubicweb-laureat-de-dataconnexions-2013
:date: 2013/02/08 09:31:54
:tags: BlogEntry
:category: Actualités

Etalab, qui coordonne l’action des services de l’Etat pour faciliter la réutilisation des données publiques, organise le concours Dataconnexions, lequel vient de récompenser CubicWeb, une plate-forme libre de développement pour le web sémantique, qui a été initiée par Logilab.

- `Compte-rendu de la journée par Etalab <://www.etalab.gouv.fr/article-dataconnexions-2-6-projets-d-ouverture-des-donnees-publiques-a-l-honneur-115339314.html>`_

- `Communiqué de presse Logilab <://www.logilab.fr/file/1884?vid=download>`_

- `Communiqué de presse de la communauté CubicWeb <https://www.cubicweb.org/file/2710828?vid=download>`_

- `Support de présentation <://slideshare.net/logilab/cubicweb-laurat-dataconnexions-2013>`_

- `Annonce sur le site de CubicWeb <://www.cubicweb.org/blogentry/2710208>`_

