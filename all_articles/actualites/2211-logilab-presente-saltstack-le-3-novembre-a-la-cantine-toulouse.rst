
Logilab présente Saltstack le 3 novembre à la cantine Toulouse
##############################################################


:slug: logilab-presente-saltstack-le-3-novembre-a-la-cantine-toulouse
:date: 2014/10/17 10:13:23
:tags: BlogEntry
:category: Actualités

Logilab vous invite le lundi 3 novembre à une présentation de
Saltstack_ à la cantine de Toulouse. Cela se passera de 18 à 20h, avec une présentation suivie
d'un moment convivial autour d'un apéritif afin de continuer la
discussion.

Plus d'information et inscription sur le `site de la cantine`_.

.. image:: ./images/"visualcantine2.jpg"

.. _Saltstack: ://www.saltstack.com/

.. _`site de la cantine`: ://lacantine-toulouse.org/6889/saltstack-une-nouvelle-approche-pour-la-gestion-de-vos-infrastructures

