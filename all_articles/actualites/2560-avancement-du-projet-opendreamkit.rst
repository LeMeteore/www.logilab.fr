
Avancement du projet OpenDreamKit
#################################


:slug: avancement-du-projet-opendreamkit
:date: 2016/04/27 17:43:28
:tags: BlogEntry
:category: Actualités

OpenDreamKit est un projet européen Horizon 2020 qui a pour objectif de faire évoluer les outils informatiques pour la recherche collaborative en mathématiques fondamentales.

.. image:: ./images/"OpenDreamKit.png"

Lors du dernier atelier de développement qui s'est tenu pendant les `77th Sage days <https://www.logilab.org/blogentry/5540528>`_, nous avons participé â l'empaquetage de SageMath pour Debian et à divers travaux sur SageMath, IPython / Jupyter, Pythran, etc.

