
Données de santé sur le Web - 23 mai 2017
#########################################


:slug: donnees-de-sante-sur-le-web-23-mai-2017
:date: 2017/05/11 11:57:42
:tags: BlogEntry
:category: Actualités

Le groupe *Paris Web of Data* organise, avec le soutien de Logilab, une soirée consacrée aux `données de santé sur le Web <https://www.meetup.com/fr-FR/paris-web-of-data/events/239108245/>`_, qui aura lieu le 23 mai 2017 dans les locaux de l'ancien hôpital St-Vincent. Le programme promet des présentations intéressantes. Il est encore temps de vous inscrire sur meetup.com.

.. image:: https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Gatunek_leczniczy_darkgreen_on_102_255_0_6C_transparent.svg/120px-Gatunek_leczniczy_darkgreen_on_102_255_0_6C_transparent.svg.png

