
Logilab sera présent au meet-up Code_Tympan !
#############################################


:slug: logilab-sera-present-au-meet-up-code-tympan
:date: 2015/09/09 14:33:08
:tags: BlogEntry
:category: Actualités

L'équipe de `Logilab <://www.logilab.fr>`_ sera présente au prochain `meet-up Code_Tympan <https://www.eventbrite.fr/e/inscription-meet-up-code-tympan-17570073563>`_  qui aura lieu mardi 22 septembre. 

`Code_TYMPAN <://code-tympan.org/>`_ est un logiciel libre d’ingénierie en acoustique environnementale.

À destination des bureaux d’études, des acousticiens et des ingénieurs, il est notamment utilisé depuis plus de 10 ans par les équipes d’EDF pour réduire l’impact sonore de leurs installations.

Les ingénieurs informaticiens de Logilab contribuent au développement du Code_Tympan.

