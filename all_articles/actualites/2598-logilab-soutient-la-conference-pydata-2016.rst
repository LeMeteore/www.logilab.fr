
Logilab soutient la conférence Pydata 2016
##########################################


:slug: logilab-soutient-la-conference-pydata-2016
:date: 2016/05/23 14:55:19
:tags: BlogEntry
:category: Actualités

Les conférences Pydata réunissent des utilisateurs et des développeurs d'outils d'analyse de données en Python. C'est l'occasion de partager des idées et d'apprendre la meilleure façon d'appliquer ce langage aux défis du vaste domaine de la gestion, du traitement, de l'analyse et de la visualisation des données.

.. image :: ./images/"pydata-logo-paris-2016.png"

**La 2ème conférence Pydata Paris aura lieu les mardi 14 et mercredi 15 juin à l'école Léonard Vinci, à la Défense.**

Pionnier (depuis 2000) et spécialiste de l'utilisation du langage Python en France, Logilab est ravie de parrainer cette conférence.

`Inscrivez-vous ! <://pydata.org/paris2016/>`_

