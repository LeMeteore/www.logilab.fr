
Logilab aux Rencontres Régionales du Logiciel Libre
###################################################


:slug: logilab-aux-rencontres-regionales-du-logiciel-libre
:date: 2016/10/03 17:37:23
:tags: BlogEntry
:category: Actualités

**Logilab vous invite à nous retrouver à la 4ème édition des Rencontres Régionales du Logiciel Libre.**

.. image :: ./images/"RRLL2016.png"

À cette occasion, `Sylvain Thénault <https://twitter.com/sythenault>`_ animera l'atelier "Gestion des données des archives, bibliothèques et musées à l'heure du web 3.0" à partir de 15h30.

Consultez le `programme et inscrivez-vous <://www.solibre.fr/fr/actualites/rejoignez-nous-aux-rrll-2016.html>`_ !

**Inscription gratuite en utilisant le code RRLL2016**

*Les RRLL de Toulouse sont inscrites dans le cadre de la manifestation Capitole du Libre organisée tous les ans par l'Association Toulibre.*


Hôtel de Région
22, Boulevard du Maréchal-Juin
31100 Toulouse Cedex 9

