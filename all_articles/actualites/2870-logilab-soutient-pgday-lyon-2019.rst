
Logilab soutient pgDay Lyon 2019
################################


:slug: logilab-soutient-pgday-lyon-2019
:date: 2019/06/24 09:24:15
:tags: BlogEntry
:category: Actualités

Nous avons eu le plaisir de soutenir l'organisation du `pgDay Lyon 2019 <https://pgday.fr/>`_ aux côté des spécialistes de Postgresql et d'autres sociétés qui en font un usage intensif.



Le programme montre que Postgresql est une base de données très flexible, qui allie performances et très grandes quantités de données !

