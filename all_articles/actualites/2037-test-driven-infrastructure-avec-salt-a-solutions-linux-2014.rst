
Test-Driven Infrastructure avec Salt à Solutions Linux 2014
###########################################################


:slug: test-driven-infrastructure-avec-salt-a-solutions-linux-2014
:date: 2014/05/14 11:56:55
:tags: BlogEntry
:category: Actualités

Logilab présentera lors de `Solutions Linux <://www.solutionslinux.fr>`_ l'avancement de ses travaux sur l'`administration système pilotée par les tests avec Salt <://www.solutionslinux.fr/animation_87_168_3490_p.html?cid=2450>`_ (Test-Driven Infrastructure). Rendez-vous le mercredi 21 mai 2014 à 9h30 au CNIT à Paris.

.. image:: https://lh5.googleusercontent.com/-_R2hTK55iJk/UPbIwcLVXqI/AAAAAAAACno/p9e_BCouKjo/s257/Linux_03.png

EDIT: `compte-rendu de la présentation <://www.logilab.org/blogentry/248099>`_

