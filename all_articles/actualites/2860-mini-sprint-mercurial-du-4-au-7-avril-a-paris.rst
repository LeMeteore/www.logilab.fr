
Mini-sprint mercurial du 4 au 7 avril à Paris
#############################################


:slug: mini-sprint-mercurial-du-4-au-7-avril-a-paris
:date: 2019/03/18 15:18:25
:tags: BlogEntry
:category: Actualités

Logilab co-organise avec la société Octobus, un mini-sprint Mercurial qui aura lieu du jeudi 4 au dimanche 7 avril à Paris.

.. image :: ./images/"New_Mercurial_logo.png"

Logilab accueillera le mini-sprint dans ses locaux parisiens les jeudi 4 et vendredi 5 avril. Octobus s'occupe des samedi et dimanche et communiquera très prochainement le lieu retenu pour ces jours-là.

Afin de participer au sprint, remplissez `le sondage <https://framadate.org/Gmwmc5mnAXBaJNQx>`_ et indiquez votre nom et les dates auxquelles vous souhaitez participer. 

Vous pouvez aussi remplir le pad pour indiquer  les thématiques que vous souhaitez aborder au cours de ce sprint : 
https://mensuel.framapad.org/p/mini-sprint-hg

Let's code together!

