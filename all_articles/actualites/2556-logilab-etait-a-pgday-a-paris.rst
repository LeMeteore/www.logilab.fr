
Logilab était à pgDay, à Paris !
################################


:slug: logilab-etait-a-pgday-a-paris
:date: 2016/04/14 18:05:49
:tags: BlogEntry
:category: Actualités

Le 31 mars 2016, `David Douard <https://twitter.com/douardda>`_ et Julien Cristau, ingénieurs développeurs chez Logilab, ont assisté à `pgDay Paris <://www.pgday.paris/>`_, journée de conférences et d'échanges organisée par la communauté française et européenne de PostgreSQL.

.. image :: ./images/"PgDay2016.png"

À cette occasion, ils ont assisté à différentes présentations dont une de Magnus Hagander sur les outils à utiliser pour faire des sauvegardes de bases, et une de Damien Clochard sur un tour d’horizon des solutions de supervision.

Découvrez leur retour en lisant leur article `Nous étions à pgDay, à Paris ! <https://www.logilab.org/blogentry/5463030>`_

