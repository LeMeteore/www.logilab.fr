
Nouvelle formation "Développement de code par le scientifique"
##############################################################


:slug: nouvelle-formation-developpement-de-code-par-le-scientifique
:date: 2014/04/29 10:58:44
:tags: BlogEntry
:category: Actualités

Forts d'une expérience de 15 ans, acquise en travaillant au croisement du développement logiciel et de la recherche scientifique, nous ajoutons à notre catalogue une formation destinée aux `scientifiques désireux d'améliorer leur utilisation de l'outil informatique <://www.logilab.fr/formations/sci-code>`_, la qualité de leur code et leur méthode de développement collaboratif.

Deux sessions inter-entreprises sont déjà prévues: en septembre à Paris et en octobre à Toulouse.

