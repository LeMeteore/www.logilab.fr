
Réunion Salt le 23 septembre 2014 à Paris
#########################################


:slug: reunion-salt-le-23-septembre-2014-a-paris
:date: 2014/08/28 17:31:46
:tags: BlogEntry
:category: Actualités

La communauté SaltStack française vous invite au meetup autour de Salt
le mardi 23 septembre 2014 de 19h à 21h à Paris dans `les locaux de
Mozilla <https://wiki.mozilla.org/Paris>`_. Lors de cette réunion des utilisateurs et développeurs de Salt,
quelques présentations éclair permettrons d'ouvrir la discussion et une
introduction à Salt pourra être faite si besoin. Salt est un
environnement d'exécution distribué et asynchrone, écrit en Python, qui
se positionne comme le couteau suisse de la gestion d'infrastructure.

Quelques comptes rendus des éditions précédentes :
://www.logilab.org/bookmark/264279/follow

.. image:: ://www.logilab.fr/file/2163/raw/saltstack_logo.png
   :width: 400px

Inscription gratuite et obligatoire sur
://framadate.org/u85rt2y3iz1krhys

L'adresse de Mozilla Paris : 16bis Boulevard Montmartre, 75009 Paris, France.

