
Nous avons été à FOSDEM et à Config Management Camp 2016
########################################################


:slug: nous-avons-ete-a-fosdem-et-a-config-management-camp-2016
:date: 2016/02/04 18:25:00
:tags: BlogEntry
:category: Actualités

Comme annoncé précédemment, `Arthur Lutz <https://www.logilab.fr/card/arthur.lutz>`_ et `David Douard <https://www.logilab.fr/card/david.douard>`_ ont participé à FOSDEM et à Config Management Camp 2016 en Belgique.

.. image :: ./images/"fosdem 2016.png"

À ces deux occasions, Arthur a présenté `Salt <://saltstack.com/community/>`_:




* à **FOSDEM**, `"Once you've configured your infrastructure using salt, monitor it by re-using that definition" <://slides.logilab.fr/2016/fosdem_describe_it_monitor_it/#/after-describing-your-infrastructure-as-code-reuse-that-to-monitor-it>`_ dans la session Configuration Management devroom



* à **Config Management Camp**, `Roll out active Supervision with Salt, Graphite and Grafana <://slides.logilab.fr/2016/cfgmgmtcamp_salt_active_supervision/#/after-describing-your-infrastructure-as-code-reuse-that-to-monitor-it>`_ dans la session Salt.

.. image :: https://www.logilab.fr/file/2488?vid=download

Découvrez `leur compte-rendu sur le blog de logilab.org <https://www.logilab.org/blogentry/4253406>`_.

Les présentations sont accessibles sur slideshare. Elles ont été filmées par les organisateurs et les vidéos seront bientôt disponibles.

