
La conférence Mercurial aura lieu mardi 28 mai, à Paris !
#########################################################


:slug: la-conference-mercurial-aura-lieu-mardi-28-mai-a-paris
:date: 2019/05/03 17:21:02
:tags: BlogEntry
:category: Actualités

Co-organisée par `Logilab <https://www.logilab.org/>`_, `Octobus <https://octobus.net/>`_ & `RhodeCode <https://rhodecode.com/>`_ la conférence Mercurial aura lieu mardi 28 mai au siège de `Mozilla  <https://en.wikipedia.org/wiki/Mozilla>`_, à Paris.

`Mercurial <https://www.mercurial-scm.org/>`_ est un système de gestion de contrôle de code source distribué gratuitement qui offre une interface intuitive pour gérer efficacement des projets de toutes tailles. Avec son système d'extension puissant, Mercurial peut facilement s'adapter à n'importe quel environnement.

.. image :: ./images/"New_Mercurial_logo.png"

`Cette première édition <https://www.mercurial.paris/>`_ s'adresse aux entreprises qui utilisent déjà Mercurial ou qui envisagent de passer d'un autre système de contrôle de version, tel que Subversion.

Assister à la conférence Mercurial permettra aux utilisateurs de partager des idées et des expériences dans différents secteurs. 
C'est aussi l'occasion de communiquer avec les principaux développeurs de Mercurial et d'obtenir des mises à jour sur le flux de travail et ses fonctionnalités modernes.

`Inscrivez-vous ! <https://www.weezevent.com/mercurial-conference-paris-2019>`_ 




Mozilla : 16 bis boulevard Montmartre 75009 - Paris

