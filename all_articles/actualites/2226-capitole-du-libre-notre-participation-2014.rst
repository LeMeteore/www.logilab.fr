
Capitole du Libre - notre participation 2014
############################################


:slug: capitole-du-libre-notre-participation-2014
:date: 2014/12/15 16:55:11
:tags: BlogEntry
:category: Actualités

Logilab, comme les années précédentes, a apporté son soutien au `Capitole du Libre à Toulouse <://2014.capitoledulibre.org/>`_
et a largement contribué à son programme en présentant `CubicWeb <://www.cubicweb.org>`_ et `SaltStack <://saltstack.org/>`_.

.. image:: ://2014.capitoledulibre.org/symposion_media/static/cdl2014/img/logo-cdl.png
  :align: center

En 2013 nous avions résumé notre travail sur 
`pylint <://www.pylint.org>`_ par `une rétrospective de 10 ans d'un communauté de logiciel 
libre <://www.logilab.org/blogentry/191368>`_ ). Cette année, en plus d'un 
stand pour rencontrer les participants à la conférence, nous étions présents lors de la session "DevOps" 
pour faire découvrir les `infrastructures pilotées par les tests avec 
SaltStack <://slides.logilab.fr/tda-salt-afpy/tda.fr.html#/>`_ (*Test-Driven Infrastructure*). 

.. image:: ://www.logilab.fr/file/2163/raw/saltstack_logo.png
  :align: center

Nous avons aussi animé un `atelier d'introduction à SaltStack <://2014.capitoledulibre.org/schedule/presentation/89/>`_
et un `atelier de développement d'application web avec CubicWeb <://2014.capitoledulibre.org/programme/presentation/86/>`_.

Merci à tous les participants de ces ateliers et aux organisateurs de cet évenement
qui devient incontournable pour le logiciel libre en dépassant chaque année un peu
plus son cadre toulousain.

.. image:: ://www.logilab.fr/file/2225/raw/logo-cubicweb.png
  :align: center

