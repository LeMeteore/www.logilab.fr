
Libre Théâtre : une bibliothèque numérique gratuite d'oeuvres théâtrales du domaine public se prépare
#####################################################################################################


:slug: libre-theatre-une-bibliotheque-numerique-gratuite-d-oeuvres-theatrales-du-domaine-public-se-prepare
:date: 2015/10/20 19:14:17
:tags: BlogEntry
:category: Actualités

`Ruth Martinez <https://twitter.com/RuthMartinez?lang=fr>`_, co-fondatrice de `Libre Théâtre <://libretheatre.fr/>`_ et ancienne déléguée générale du GFII, a répondu aux 3 questions du journaliste Bruno Texier de l'ArchiMag.com.

Libre Théâtre - dont Logilab est partenaire technique - combine l’open data et le web sémantique.

Découvrez l'`article <://www.archimag.com/veille-documentation/2015/10/20/libre-theatre-bibliotheque-numerique-gratuite-oeuvres-theatrales>`_.

