
Libre Théâtre au Forum des Archivistes
######################################


:slug: libre-theatre-au-forum-des-archivistes
:date: 2016/03/21 18:02:43
:tags: BlogEntry
:category: Actualités

Ruth Martinez, en charge du pilotage du projet `LibreThéâtre <https://twitter.com/libretheatre>`_, bibliothèque numérique des œuvres théâtrales du domaine public, présentera ce `portail <://libretheatre.fr/>`_ lors du `Forum des Archivistes <://forum2016.archivistes.org/>`_ mercredi 30 mars à Troyes, à l'occasion de la table ronde « Open data : promesses, prouesses et compromis », animée par Jean-Marie Bourgogne.


.. image :: ./images/"AAFTroyes2016.png"



**Le Forum des Archivistes meta/morphoses : les archives, bouillons de culture numérique aura lieu du 30 mars au 1er avril, à Troyes.**


Les données mises à disposition par Libre Théâtre sont publiées grâce à une application CubicWeb développée par nos soins.

