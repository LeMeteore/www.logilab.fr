
Le meetup Python, c'est ce soir !
#################################


:slug: le-meetup-python-c-est-ce-soir
:date: 2017/10/03 17:48:45
:tags: BlogEntry
:category: Actualités

Membre toujours actif de la communauté Python, Logilab soutient le *meetup Python Nantes sur luigi et behave* qui aura lieu ce soir, à 19h au site de Voyages-Sncf.com situé au sud de la Gare SNCF, au 5ème étage du bâtiment Jalais dont l'entrée principale se trouve au 34 rue du Pré Gauchet.

.. image :: ./images/"meetup_newlogo.png"

**Entrée gratuite, mais inscription obligatoire.**

`INSCRIVEZ-VOUS ! <https://www.meetup.com/fr-FR/preview/Nantes-Python-Meetup/events/243269909>`_

