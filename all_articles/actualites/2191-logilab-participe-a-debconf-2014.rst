
Logilab participe à Debconf 2014
################################


:slug: logilab-participe-a-debconf-2014
:date: 2014/08/22 09:04:30
:tags: BlogEntry
:category: Actualités

Du 23 au 31 août, à Portland (USA), se déroulera la conférence annuelle autour de la distribution Debian, la `Debconf
<://debconf14.debconf.org/talks.xhtml>`_. Logilab continue
d'apporter son soutien à cet évenement annuel en étant `sponsor de cette édition <://debconf14.debconf.org/sponsors.xhtml>`_. Logilab y sera représentée par `Julien Cristau <https://qa.debian.org/developer.php?login=julien.cristau%40logilab.fr>`_ qui y présentera deux sujets :

* pour le *track* `cloud <https://summit.debconf.org/debconf14/track/cloud/>`_ : l'utilisation de `SaltStack <://www.saltstack.org>`_ sur les systèmes Debian (`cf. description <https://summit.debconf.org/debconf14/meeting/91/putting-some-salt-in-your-debian-systems/>`_),

* pour le *track* `debian teams <https://summit.debconf.org/debconf14/track/debian-teams/>`_ : un point d'étape sur la sortie de la prochaine version de Debian (`description <https://summit.debconf.org/debconf14/meeting/31/jessie-bits-from-the-release-team/>`_).

(maj: lisez notre `compte-rendu <://www.logilab.org/blogentry/264660>`_ de la conférence)

.. image:: ://www.logilab.org/file/263602/raw/debconf2014.png
  :align: center

Logilab utilise `Debian <://www.debian.org>`_ comme principale
distribution Linux depuis la création de la société, et y contribue activement en mettant à disposition et maintenant de nombreux paquets pour cette distribution. Logilab propose plusieurs `formations Debian <https://www.logilab.fr/formations/>`_.

Logilab a choisi SaltStack pour la gestion de son infrastructure,
participe à son développement, et propose du `conseil et de la formation <://www.logilab.fr/formations/salt>`_ pour maîtriser rapidement cet outil.

