
Logilab, une pépite du pôle Systematic
######################################


:slug: logilab-une-pepite-du-pole-systematic
:date: 2014/09/01 14:44:27
:tags: BlogEntry
:category: Actualités

`Simulagora <https://www.simulagora.com>`_, l'application de simulation numérique collaborative développée par Logilab, a obtenu la troisième place du `prix de l'innovation <://www.events-systematic-paris-region.org/#!les-innovations/c23lb>`_ attribué par le public lors de la dernière convention du pôle de compétitivité Systematic en juin 2014 à Paris.

Suite à cet événement, Logilab a été présentée par `Le Monde Informatique <://www.lemondeinformatique.fr/actualites/lire-systematic-bilan-et-perspectives-du-soutien-au-logiciel-libre-58108.html>`_ et `Silicon.fr <://www.silicon.fr/systematic-met-en-lumiere-ses-champions-du-monde-open-source-95717.html>`_ comme l'une des pépites du groupe Logiciel Libre de Systematic.

.. image:: //www.logilab.fr/file/1410/raw/systematic.png
   width: 400px

