
Prochaines sessions de formation
################################


:slug: prochaines-sessions-de-formation
:date: 2014/04/08 18:26:56
:tags: BlogEntry
:category: Actualités

Le calendrier des prochaines sessions de formation Python, Debian, Salt ou encore Mercurial a été mis à jour. 

Demandez `le programme`_ !

.. _`le programme`: ://www.logilab.fr/formations

