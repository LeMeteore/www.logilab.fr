
OpenCat - un catalogue de bibliothèque fondé sur data.bnf.fr
############################################################


:slug: opencat-un-catalogue-de-bibliotheque-fonde-sur-data-bnf-fr
:date: 2013/12/17 13:26:47
:tags: BlogEntry
:category: Actualités

Le projet `OpenCat <://www.bnf.fr/fr/professionnels/modelisation_ontologies/a.opencat.html>`_ a permis de préfigurer ce que seront d'ici quelques années les catalogues de bibliothèques qui réutiliseront les données disponibles sur le web et en particulier sur `data.bnf.fr <://data.bnf.fr>`_.

Soutenue par le ministère de la Culture et menée en partenariat avec la bibliothèque nationale de France et la bibliothèque municipale de Fresnes, cette expérimentation a abouti à un démonstrateur dans lequel il est possible de créer son propre catalogue en y déposant une liste de livres. Les éléments de la liste sont automatiquement mis en relation avec les ressources issues de data.bnf.fr. On obtient un catalogue navigable en ligne qui utilise des identifiants uniques et pérennes (numéro d'`ARK <://fr.wikipedia.org/wiki/Archival_Resource_Key_%28ARK%29>`_ de la BnF), qui dispose de notices de qualité décrivant les auteurs, les oeuvres et les thèmes (`modélisation FRBR <://fr.wikipedia.org/wiki/FRBR>`_), et qui inclut des liens vers des ressources externes (wikipedia, gallica, conférences, etc).

A l'avenir, de nombreux vendeurs de systèmes d'information de gestion de bibliothèque intégreront des fonctionnalités de ce type, ce qui permettra de réduire les coûts liés à la gestion des catalogues tout en profitant des ressources disponibles sur le web.


Pour être accompagnés dans votre réflexion sur le potentiel du web sémantique au sein des bibliothèques, archives et musées, faites appel nos experts qui ont aidé la BnF dans la mise en place de data.bnf.fr et assurent des prestations de `conseil et de formation </formations/semweb-intro>`_.

.. image:: ://www.logilab.fr/file/1982/raw/opencat-schema.png

