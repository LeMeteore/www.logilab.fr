
Mise en ligne des données Brainomics/Localizer
##############################################


:slug: mise-en-ligne-des-donnees-brainomics-localizer
:date: 2013/12/04 00:00:00
:tags: BlogEntry
:category: Actualités

`Neurospin <://www-dsv.cea.fr/neurospin/>`_ et ses partenaires de recherche dans le domaine des neurosciences et de la bioinformatique ont le plaisir d'annoncer la mise en ligne du jeu de données `Brainomics/Localizer <://brainomics.cea.fr/localizer/>`_.

Le projet Brainomics_, qui bénéfice du soutien de l'Agence nationale de la recherche, fait avancer l'état de l'art de la `fusion des données de neuro-imagerie et de génomique <://www.cubicweb.org/blogentry/3123354>`_.

La publication du jeu de données Brainomics/Localizer  est une première, qui facilitera le travail des **scientifiques qui tentent de comprendre le fonctionnement du cerveau humain**. Les données sont accessibles via une application disponible sous forme de logiciel libre et construite sur la base de CubicWeb_ par Logilab.

.. _CubicWeb: ://www.cubicweb.org
.. _Brainomics: ://www.brainomics.net/

.. image:: https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/CCN.png/240px-CCN.png
    :align: center

