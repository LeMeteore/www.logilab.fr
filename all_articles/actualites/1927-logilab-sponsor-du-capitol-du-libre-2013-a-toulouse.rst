
Logilab sponsor du Capitol du Libre 2013 à Toulouse
###################################################


:slug: logilab-sponsor-du-capitol-du-libre-2013-a-toulouse
:date: 2013/10/01 11:36:15
:tags: BlogEntry
:category: Actualités

Logilab soutient le Logiciel Libre à destination des professionnels mais également du grand public, en étant sponsor de l'édition 2013 du  `Capitol du Libre`_.

À cette occasion, Sylvain Thénault, directeur de notre agence locale, animera une mini-conférence le samedi et un atelier le dimanche autour de Pylint_, l'analyseur de code Python développé par Logilab depuis plus de 10 ans.

Venez nombreux !

.. _`Capitol du Libre`: ://2013.capitoledulibre.org/sponsors.html

.. _Pylint: ://www.pylint.org

.. image:: ://capitoledulibre.org/2013/theme/images/banniere-cdl2013.png
 :width: 340pt

