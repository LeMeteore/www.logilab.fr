
Logilab officiellement membre d'Aerospace Valley
################################################


:slug: logilab-officiellement-membre-d-aerospace-valley
:date: 2014/01/08 16:27:40
:tags: BlogEntry
:category: Actualités

Logilab vient de rejoindre officiellement le `pôle de
compétitivité Aerospace Valley`_ après quelques mois
d'interactions avec ce pôle si dynamique en régions Midi-Pyrénées et Aquitaine.

Nous avons hâte de collaborer activement avec les quelques 680
membres du pôle pour apporter à l'écosystème du Sud-Ouest notre
vitalité et notre expertise !

.. image:: ./images/"av-logo.png"

.. _`pôle de compétitivité Aerospace Valley`: ://www.aerospace-valley.com/

