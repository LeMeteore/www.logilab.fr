
Logilab présent au Hackathon Code_TYMPAN
########################################


:slug: logilab-present-au-hackathon-code-tympan
:date: 2015/10/05 10:53:46
:tags: BlogEntry
:category: Actualités

Suite au meet-up de `Code_Tympan <://www.code-tympan.org/index.php/description>`_ qui a eu lieu en septembre, les ingénieurs de Logilab seront présents au `Hackathon Code_Tympan <://www.eventbrite.fr/e/billets-hackathon-code-tympan-17570450691?aff=eac2>`_.

Contributeurs au développement de `Code_Tympan <://www.code-tympan.org/index.php/description>`_, venez améliorer le code existant et développer des nouvelles fonctionnalités. 

Notre équipe vous donne rendez-vous lundi 5 octobre et mardi 6 octobre à 09:30 au 15 rue Poissonnière 75002 Paris.

Pour plus d'informations, écrivez à contact@logilab.fr

