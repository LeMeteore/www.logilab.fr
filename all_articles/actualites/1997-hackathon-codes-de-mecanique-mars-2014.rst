
Hackathon codes de mécanique (mars 2014)
########################################


:slug: hackathon-codes-de-mecanique-mars-2014
:date: 2014/02/05 11:56:12
:tags: BlogEntry
:category: Actualités

Logilab organise dans ses locaux parisiens le 27 mars 2014 un `hackathon <://fr.wikipedia.org/wiki/Hackathon>`_ consacré aux logiciels libres de simulation des phénomènes mécaniques.

Des membres des équipes de développement de Code_Aster_ et LMGC90_ ont annoncé leur participation. Le but est d'explorer des voies de collaboration entre les projets et des changement d'architecture réalisables à moyen terme.

La journée est ouverte à tous, vous trouverez plus de détails sur le `wiki
<://www.code-aster.org/wiki/doku.php?id=en:p03_dev:hackathon_02>`_.

.. _LMGC90: ://www.lmgc.univ-montp2.fr/LMGC90
.. _Code_Aster: ://www.code-aster.org

