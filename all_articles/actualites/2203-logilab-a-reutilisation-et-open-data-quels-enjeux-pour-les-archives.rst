
Logilab à "Réutilisation et open data : quels enjeux pour les archives ?"
#########################################################################


:slug: logilab-a-reutilisation-et-open-data-quels-enjeux-pour-les-archives
:date: 2014/09/22 11:19:11
:tags: BlogEntry
:category: Actualités

Nous participerons à la journée d'étude sur les `enjeux de l'open data pour les archives <://portail-joconde.over-blog.com/article-journee-d-etude-paris-23-09-2014-reutilisation-et-open-data-quels-enjeux-pour-les-archives-124500800.html>`_ qui aura lieu mardi 23 septembre 2014 à l'`Institut National du Patrimoine <://www.inp.fr/>`_. Le `programme <://www.inp.fr/index.php/fr/recherche_colloques_et_editions/manifestations_scientifiques/colloques/programme/reutilisation_et_open_data_quels_enjeux_pour_les_archives>`_ inclut une présentation de `data.bnf.fr <://data.bnf.fr>`_, que nous avons réalisé.

.. image:: ://a141.idata.over-blog.com/210x300/4/52/97/94/reut.jpg

