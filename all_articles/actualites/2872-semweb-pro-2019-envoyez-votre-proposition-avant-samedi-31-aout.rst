
SemWeb.Pro 2019 : envoyez votre proposition avant samedi 31 août !
##################################################################


:slug: semweb-pro-2019-envoyez-votre-proposition-avant-samedi-31-aout
:date: 2019/06/28 17:28:31
:tags: BlogEntry
:category: Actualités

La prochaine édition de SemWeb.Pro aura lieu mardi 3 décembre au FIAP Jean Monnet, à Paris.

.. image :: ./images/"swep_2018_newsletter.png"

Nous vous invitons à soumettre vos propositions de présentation en `répondant à l'appel à communication <://www.semweb.pro/semwebpro-2019.html>`_ **avant le 31 août 2019**. 

Pour être tenu informé de l'ouverture de la billetterie, envoyez un courriel à contact@semweb.pro en demandant à être inscrit à la liste d'information.

