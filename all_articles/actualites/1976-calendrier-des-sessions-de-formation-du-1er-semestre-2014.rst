
Calendrier des sessions de formation du 1er semestre 2014
#########################################################


:slug: calendrier-des-sessions-de-formation-du-1er-semestre-2014
:date: 2013/12/13 16:58:02
:tags: BlogEntry
:category: Actualités

Nous venons de publier le calendrier des sessions de formation inter-entreprises pour le 1er semestre 2014.

Pas mal de nouveautés au programme, notamment la nouvelle mouture de notre formation `Administration système avancée </formations/debian-admin-avance>`_, ainsi que les 1eres sessions inter-entreprises des formations `Gestion de source avec Mercurial </formations/hg-base>`_, `Gestion d'infrastructure avec Salt </formations/salt-base>`_, `Python pour l'analyse de données </formations/python-num-data>`_ et enfin `Apprentissage statistique et fouille de données avec Python </formations/python-learn>`_ !

Nous espérons que vous serez nombreux à découvrir et apprécier ces formations à la pointe de la technique.

rql`Any DATE(D),DATE(ED),L,E ORDERBY D WHERE E is Event, E concerns F, E diem D, E diem >= '2014-01-01', E diem <= '2014-06-30', E location L, E end_date ED:table`

