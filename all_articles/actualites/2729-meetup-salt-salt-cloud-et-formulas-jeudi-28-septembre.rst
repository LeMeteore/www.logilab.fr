
Meetup salt, salt-cloud et formulas : jeudi 28 septembre
########################################################


:slug: meetup-salt-salt-cloud-et-formulas-jeudi-28-septembre
:date: 2017/09/19 17:35:10
:tags: BlogEntry
:category: Actualités

.. image :: ./images/"meetup-salt.png"

Pour ce meetup de rentrée, trois présentations au programme :

• Introduction à SaltStack et son écosystème

• Quelles nouveautés dans la version 2017.7 de Salt Nitrogen ?

• Utiliser les formulas pour déployer des composants logiciels sur le cloud (avec salt-cloud) 

`INSCRIVEZ-VOUS <https://www.meetup.com/fr-FR/Paris-Salt-Meetup/events/243390968/>`_

*Heuritech : 248 rue du faubourg Saint Antoine, Paris.*

