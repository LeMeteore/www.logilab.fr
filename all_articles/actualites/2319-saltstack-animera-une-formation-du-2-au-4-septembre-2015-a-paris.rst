
SaltStack animera une formation du 2 au 4 septembre 2015 à Paris
################################################################


:slug: saltstack-animera-une-formation-du-2-au-4-septembre-2015-a-paris
:date: 2015/07/10 16:29:53
:tags: BlogEntry
:category: Actualités

Notre partenaire SaltStack vient d'annoncer sa première formation Salt à Paris, qui aura lieu du 2 au 4 septembre 2015 dans nos locaux.

Lisez l'annonce_ pour les détails et les inscriptions à l'examen de certification.

.. _annonce: ://saltstack.com/saltstack-training-paris/

