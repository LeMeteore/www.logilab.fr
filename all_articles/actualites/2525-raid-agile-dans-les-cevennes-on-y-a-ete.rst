
Raid Agile dans les Cévennes, on y a été !
##########################################


:slug: raid-agile-dans-les-cevennes-on-y-a-ete
:date: 2016/02/17 13:53:00
:tags: BlogEntry
:category: Actualités

**Chez Logilab nous appliquons les méthodes agiles à nos développements et à tous nos projets.**

Afin d'approfondir nos connaissances, une partie de notre équipe a participé au `quatrième raid agile <://raidagile.fr/raid4.html>`_ organisé par `Claude Aubry <://www.aubryconseil.com/>`_ et `Pablo Pernot <://www.areyouagile.com/>`_ dans les Cévennes.


.. image :: ./images/"RaidAgile.png"


`Découvrez leur retour ! <https://www.logilab.org/blogentry/4343091>`_

