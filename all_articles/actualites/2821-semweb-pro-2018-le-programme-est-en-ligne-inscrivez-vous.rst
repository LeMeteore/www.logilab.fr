
SemWeb.Pro 2018 : le programme est en ligne, inscrivez-vous !
#############################################################


:slug: semweb-pro-2018-le-programme-est-en-ligne-inscrivez-vous
:date: 2018/09/13 16:50:25
:tags: BlogEntry
:category: Actualités

.. image :: ./images/"SemWebPro_inscriptions.jpeg"


**Mardi 6 novembre au FIAP Jean Monnet, à Paris**

Consultez le `programme et inscrivez-vous <://semweb.pro/semwebpro-2018.html>`_ dès à présent afin de
bénéficier du tarif à 70€ (passage à 120€ après le 15 octobre). 

*Vous pouvez assister à cette journée dans le cadre d'une
formation professionnelle (donnant lieu à l'établissement d'une
convention de formation). Dans ce cas, le tarif applicable est de
250€.*

Suivez nos actualités sur Twitter `@semwebpro <https://twitter.com/semwebpro>`_ mais aussi avec le hashtag `#semwebpro <https://twitter.com/hashtag/Semwebpro>`_

Pour plus d'informations, contactez-nous : contact@semweb.pro

