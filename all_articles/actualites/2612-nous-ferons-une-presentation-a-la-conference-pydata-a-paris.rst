
Nous ferons une présentation à la conférence Pydata à Paris
###########################################################


:slug: nous-ferons-une-presentation-a-la-conference-pydata-a-paris
:date: 2016/06/02 18:38:53
:tags: BlogEntry
:category: Actualités

`Olivier Cayrol <https://twitter.com/OCayrol>`_ sera présent à la `conférence Pydata <://pydata.org/paris2016/>`_ qui aura lieu mardi 14 et mercredi 15 juin à la Défense, à Paris.

.. image :: ./images/"pydata-logo-paris-2016.png"

À cette occasion, Olivier présentera "Using Python to revolutionize the musical instruments manufacturing".

Consultez le `programme  <://pydata.org/paris2016/schedule/>`_ et `inscrivez-vous <://pydata.org/paris2016/register/>`_.

