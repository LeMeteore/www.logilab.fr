
Logilab partenaire technique de Libre Théâtre
#############################################


:slug: logilab-partenaire-technique-de-libre-theatre
:date: 2015/07/28 16:40:18
:tags: BlogEntry
:category: Actualités

Un partenariat technique vient d'être conclu entre Logilab et `Libre Théâtre <://libretheatre.fr/>`_. Libre Théâtre facilite l’accès gratuit aux textes de théâtre français libres de droit. Logilab apporte au projet son expertise dans la réalisation de sites intégrés au web des données et dans le traitement de documents structurés.

Un démonstrateur alimenté avec les métadonnées d'un corpus
des 500 pièces permet d’ores et déjà d’illustrer les
possibilités variées de recherche. Une première version sera mise en
ligne en septembre sur le site ://libretheatre.fr

